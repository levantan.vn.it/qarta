@extends('admin.templates.master')

@section('content')
<section class="content-header">
    <h1>About Dining</h1>
    <ol class="breadcrumb">
        <li><a href="{{Helper::url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">About Dining</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">About Dining</h3>
                </div>
                {!! Form::open(['url' => Helper::url('admin/dining/submit_about'), 'id' => 'submitForm']) !!}
                <div class="box-body">
                    <div class="form-group">
                        <label>Title:</label>
                        <input class="form-control" name="dining_title" value="{{$config['dining_title']}}" required="" />
                    </div>
                    <div class="form-group">
                        <label>Vi Title:</label>
                        <input class="form-control" name="dining_title_vi" value="{{$config_vi['dining_title']}}" required />
                    </div>
                    <div class="form-group">
                        <label>Sub Title:</label>
                        <input class="form-control" name="dining_sub_title" value="{{$config['dining_sub_title']}}" required/>
                    </div>
                    <div class="form-group">
                        <label>Vi Sub Title:</label>
                        <input class="form-control" name="dining_sub_title_vi" value="{{$config_vi['dining_sub_title']}}" required/>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
    $('#submitForm').validate({
        ignore: [],
        rules: {
        },
        messages: {
        },
        submitHandler: function(form) {
            var submit_url = $('#submitForm').attr('action');
            $.ajax({
                type: "POST",
                url: submit_url,
                data: $(form).serialize(),
                dataType: "JSON",
                beforeSend : function(){
                    $('#loading').show();
                },
                success: function(result){
                    $('#loading').hide();
                    if(result.success){
                        location.reload();
                        return false;
                    }
                    else{
                        modalError(result.message);
                        return false;
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    $('#loading').hide();
                    modalError(jqXHR.status +' '+errorThrown+'. Please reload and try agian. Thank you!!');
                    // modalError(xhr.responseText);
                }
            });
            return false;
        }
    });
});
</script>
@stop
