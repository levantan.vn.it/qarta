<footer class="main-footer">
    <strong>Copyright {{date('Y')}}  &copy; ABC &reg; </strong>  All rights reserved.
</footer>
<div class="modal fade">
    <div class="modal-dialog">
        <div class="box box-primary">
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
<style type="text/css">
    #loading {z-index: 999999;position: fixed;width: 100%;height: 100%;left: 0;top: 0;background-color: rgba(0, 0, 0, 0.6);}
    .svg-icon-loader {width: 100px;height: 100px;float: left;line-height: 100px;text-align: center;}
    #loading .svg-icon-loader {position: absolute; top: 50%;left: 50%;margin: -50px 0 0 -50px;
    }
    #loading .svg-icon-loader img{
        width: 100px!important
    }
</style>
<div id="loading" style="display: none;">
    <div class="svg-icon-loader">
        <img src="{{url('assets/img/curved-bar.gif')}}" width="200px" alt="">
    </div>
</div>