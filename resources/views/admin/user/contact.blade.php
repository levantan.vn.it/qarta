@extends('admin.templates.master')

@section('content')
<section class="content-header">
    <h1>Contact Us</h1>
    <ol class="breadcrumb">
        <li><a href="{{url('')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Contact Us</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-success alert-dismissable" style="display: none;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                Submit a query form to admin success.
            </div>
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Contact Us</h3>
                </div>
                {!! Form::open(['url' => Helper::url('contact'), 'id' => 'submitForm']) !!}
                <div class="box-body">
                    <div class="form-group">
                        <label>Subject:</label>
                        <input class="form-control" name="subject" value="" required="" />
                    </div>
                    <div class="form-group">
                        <label>Message:</label>
                        <textarea class="form-control" name="content" required rows="4"></textarea>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
    $('#submitForm').validate({
        ignore: [],
        rules: {
        },
        messages: {
        },
        submitHandler: function(form) {
            var submit_url = $('#submitForm').attr('action');
            $.ajax({
                type: "POST",
                url: submit_url,
                data: $(form).serialize(),
                dataType: "JSON",
                beforeSend : function(){
                    $('#loading').show();
                },
                success: function(result){
                    $('#loading').hide();
                    if(result.success){
                        // location.reload();
                        $('.alert-success').show();
                        $('input').val('');
                        $('textarea').val('');
                        return false;
                    }
                    else{
                        modalError(result.message);
                        return false;
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    $('#loading').hide();
                    modalError(jqXHR.status +' '+errorThrown+'. Please reload and try agian. Thank you!!');
                    // modalError(xhr.responseText);
                }
            });
            return false;
        }
    });
});
</script>
@stop
