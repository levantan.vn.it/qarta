@extends('admin.templates.master')

@section('content')
<link rel="stylesheet" href="{{Helper::getThemePlugins('dmuploader/css/demo.css')}}" />
<link rel="stylesheet" href="{{Helper::getThemePlugins('dmuploader/css/uploader.css')}}" />
<section class="content-header">
    <h1>{{!empty($item)?'Cập nhật trả lời':'Thêm mới'}}</h1>
    <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('admin/hoi-dap')}}"> Danh sách</a></li>
        <li class="active">{{!empty($item)?'Cập nhật trả lời':'Thêm mới'}}</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">{{!empty($item)?'Cập nhật trả lời':'Thêm mới'}}</h3>
                </div>
                <div class="box-body">
                    <div class="col-xs-12">
                        {!! Form::open(['url' => url('admin/hoi-dap/save'), 'id' => 'submitForm']) !!}
                        <div class="form">
                            @if(!empty($item))
                            <input type="hidden" name="id" value="{{$item->id}}">
                            <p>Câu hỏi: {{$item->question}}</p>
                            <div class="form_row form-group">
                                <label>Trả lời:</label>
                                <textarea rows="4" class="form_input form-control" name="answer">{{$item->answer}}</textarea>
                            </div>
                            @else
                            <div class="form_row form-group">
                                <label>Tên:</label>
                                <input type="text" class="form_input form-control" name="name" value="{{!empty($item)?$item->name:''}}">
                            </div>
                            <div class="form_row form-group">
                                <label>Phone:</label>
                                <input type="text" class="form_input form-control" name="name" value="{{!empty($item)?$item->name:''}}">
                            </div>
                            <div class="form_row form-group">
                                <label>Email:</label>
                                <input type="email" class="form_input form-control" name="name" value="{{!empty($item)?$item->name:''}}">
                            </div>
                            <div class="form_row form-group">
                                <label>Câu hỏi:</label>
                                <textarea rows="4" class="form_input form-control" name="question"></textarea>
                            </div>
                            <div class="form_row form-group">
                                <label>Trả lời:</label>
                                <textarea rows="4" class="form_input form-control" name="answer"></textarea>
                            </div>
                            @endif
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
$(document).ready(function(){
    
    $('#submitForm').validate({
        ignore: [],
        rules: {
        },
        messages: {
        },
        submitHandler: function(form) {
            var submit_url = $('#submitForm').attr('action');
            $.ajax({
                type: "POST",
                url: submit_url,
                data: $(form).serialize(),
                dataType: "JSON",
                beforeSend : function(){
                    $('#loading').show();
                },
                success: function(result){
                    $('#loading').hide();
                    if(result.success){
                        location.replace("{{url('admin/san-pham/danh-muc')}}");
                        return false;
                    }
                    else{
                        modalError(result.message);
                        return false;
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    $('#loading').hide();
                    modalError(jqXHR.status +' '+errorThrown+'. Please reload and try agian. Thank you!!');
                    // modalError(xhr.responseText);
                }
            });
            return false;
        }
    });
});
</script>
@stop
