<!DOCTYPE html>
<html>
<head>
    <title>MANAGEMENT SYSTEM</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <!-- <link type="image/x-icon" href="{{Helper::getThemeImg('logo/favicon.ico')}}" rel="shortcut icon"> -->
    <meta name="robots" content="noindex, nofollow" />
    <meta name="googlebot" content="noindex, nofollow" />
    <meta charset="utf-8" name="_token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{url('assets/css/admin/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{url('assets/css/admin/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{url('assets/css/admin/ionicons.min.css')}}" />
    <link rel="stylesheet" href="{{url('assets/plugins/iCheck/all.css')}}" />
    <link rel="stylesheet" href="{{url('assets/plugins/messi/messi.min.css')}}" />
    <link rel="stylesheet" href="{{url('assets/plugins/select2/select2.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/admin/AdminLTE.min.css')}}" />
    <link rel="stylesheet" href="{{url('assets/css/admin/skins/skin-blue.min.css')}}" />
    <link rel="stylesheet" href="{{url('assets/plugins/datatables/dataTables.bootstrap.css')}}" />
    <link rel="stylesheet" href="{{url('assets/plugins/datetimepicker/jquery.datetimepicker.css')}}" />
    <link rel="stylesheet" href="{{url('assets/plugins/dropzone/dist/min/dropzone.min.css')}}" />
    <link rel="stylesheet" href="{{url('assets/plugins/select2/select2.min.css')}}">
    @yield('css_wrapper')
    <link rel="stylesheet" href="{{url('assets/css/admin/style.css?v='.time())}}"/>
    <script src="{{url('assets/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
</head>
<body class="hold-transition skin-blue fixed sidebar-mini">
    <div class="wrapper">
        @include('admin.templates.header')
        @include('admin.templates.sidebar')
        <div class="content-wrapper" style="min-height: 916px;">
            @yield('content')
        </div>
        @include('admin.templates.footer')
        @yield('aside')
    </div>
    <script src="{{url('assets/js/admin/bootstrap.min.js')}}"></script>
    <script src="{{url('assets/plugins/messi/messi.js')}}"></script>
    <script src="{{url('assets/plugins/iCheck/icheck.min.js')}}"></script>
    <script src="{{url('assets/js/admin/jquery.validate.min.js')}}"></script>
    <script src="{{url('assets/plugins/slimScroll/jquery.slimscroll.js')}}"></script>
    <script src="{{url('assets/js/admin/app.js')}}"></script>
    <script src="{{url('assets/js/admin/admin.js')}}"></script>
    <script src="{{url('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{url('assets/plugins/datetimepicker/jquery.datetimepicker.full.min.js')}}"></script>
    <script type="text/javascript">
        var admin_url = window.location.origin+'/admin';
    </script>
    @yield('js_wrapper')
    <script type="text/javascript">
    $(document).ready(function(){
        $(document).on("click",'.load-modal-content', function(e) {
            e.preventDefault();
            var obj = $(this);
            loadModalContent(obj);
        });
    });
    </script>
</body>
</html>