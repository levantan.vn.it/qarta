<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
    <link rel="stylesheet" href="{{Helper::getThemeCss('admin/bootstrap.min.css')}}"/>
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"/> -->
    <link rel="stylesheet" href="{{Helper::getThemeCss('admin/font-awesome.min.css')}}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"/>
    <link rel="stylesheet" href="{{Helper::getThemeCss('admin/AdminLTE.min.css')}}"/>
    <link rel="stylesheet" href="{{Helper::getThemePlugins('iCheck/square/blue.css')}}"/>
    <link rel="stylesheet" href="{{Helper::getThemeCss('admin/style.css')}}"/>



    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            Login management system
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">Sign in to start your session</p>
            {!! Form::open(['url' => Helper::url('login'), 'id' => 'loginForm']) !!}
            <div class="form-group">
                <input type="text" name="email" class="form-control" placeholder="Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Passwword">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
            </div>
            {!! Form::close() !!}
            <a href="{{url('register')}}" style="display: block;padding-top: 10px;">Register a new membership</a>
            <a href="{{url('contact')}}" style="display: block;">Contact us</a>
        </div>
    </div>
    <style type="text/css">
        #loading {z-index: 999999;position: fixed;width: 100%;height: 100%;left: 0;top: 0;background-color: rgba(0, 0, 0, 0.6);}
        .svg-icon-loader {width: 100px;height: 100px;float: left;line-height: 100px;text-align: center;}
        #loading .svg-icon-loader {position: absolute; top: 50%;left: 50%;margin: -50px 0 0 -50px;
        }
        #loading .svg-icon-loader img{
            width: 100px!important
        }
    </style>
    <div id="loading" style="display: none;">
        <div class="svg-icon-loader">
            <img src="{{Helper::getThemeImg('curved-bar.gif')}}" width="200px" alt="">
        </div>
    </div>
    <script src="{{Helper::getThemePlugins('jQuery/jQuery-2.1.4.min.js')}}"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script> -->
    <script src="{{Helper::getThemeJs('admin/bootstrap.min.js')}}"></script>
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> -->
    <script src="{{Helper::getThemePlugins('iCheck/icheck.min.js')}}"></script>
    <script src="{{Helper::getThemeJs('admin/jquery.validate.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%'
            });

            $('#loginForm').validate({
                ignore: [],
                rules: {
                    email: {
                        required: true,
                        email: true,
                    },
                    password: {
                        required: true,
                        maxlength: 20,
                        minlength:6
                    }
                },
                messages: {
                },
                submitHandler: function (form) {
                    var submit_url = $('#loginForm').attr('action');
                    $.ajax({
                        url: submit_url,
                        type: 'POST',
                        dataType: 'JSON',
                        data: $(form).serialize(),
                        beforeSend : function(){
                            $('#loading').show();
                            $('.btn-primary').addClass('disabled');$('.btn-primary').attr('disabled','disabled');
                        },
                        success: function(str) {
                            $('#loading').hide();$('.btn-primary').removeClass('disabled');$('.btn-primary').removeAttr('disabled');
                            $('.form-group').removeClass('has-error');
                            if (!str.success) {
                                $('.login-box-msg').addClass('text-red').text(str.message);
                                $('#' + str.flag).parent().addClass('has-error');
                            } else {
                                $('.login-box-msg').addClass('text-green').text(str.message);
                                window.location.replace('{{  Helper::url("")  }}');
                            }
                            return false;
                        }
                    });
                    return false;
                }
            });
        });
    </script>

</body>
</html>