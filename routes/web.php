<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('login', 'LoginController@index');
Route::group(['prefix' => ''], function () {
	Route::get('/','AdminController@index');
	Route::get('logout', 'AdminController@logout');
	Route::post('submit_change_password', 'AdminController@submit_change_password');
	Route::get('change_password', 'AdminController@change_password');
	Route::get('login', 'LoginController@index');
	Route::get('register', 'LoginController@register');
	Route::post('login', 'LoginController@submit');
	Route::post('register', 'LoginController@submit_register');
	Route::post('get_state', 'LoginController@get_state');
	Route::post('checkUser', 'LoginController@checkUser');
	Route::post('checkEmail', 'LoginController@checkEmail');
	Route::get('thank-you', 'LoginController@thank_you');
	Route::get('verify-account/{key}', 'LoginController@active_key');
	Route::get('user','AdminController@user');
	Route::post('updateStatus', 'AdminController@updateStatus');
	Route::post('removeUser', 'AdminController@removeUser');
	Route::get('user/view/{id}', 'AdminController@view_user');
	Route::get('user/edit/{id}', 'AdminController@edit_user');
	Route::post('edit_user', 'AdminController@submit_edit_user');
	Route::get('sendmail','AdminController@sendmail');
	Route::get('contact','AdminController@contact');
	Route::post('contact','AdminController@submit_contact');

});

