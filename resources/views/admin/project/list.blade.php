@extends('admin.templates.master')

@section('content')

<section class="content-header">
    <h1>{{Helper::get_type($category)}}</h1>
    <ol class="breadcrumb">
        <li><a href="{{Helper::url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{Helper::get_type($category)}}</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">{{Helper::get_type($category)}}</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped table-data">
                        <thead>
                            <tr>
                                <th>Tiêu đề</th>
                                <th>Ảnh đại diện</th>
                                <th>Ngày tạo</th>
                                <th>Nổi bật</th>
                                <th>Trạng thái</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(!empty($item))
                            @foreach ($item as $key)
                                <tr>
                                    <td>{{$key->title}}
                                        <div class="row-actions">
                                            <span class="edit">
                                                @if($category == 1)
                                                <a href="{{Helper::url('admin/du-an/edit/'.$key->id)}}">Edit</a> 
                                                @elseif($category == 2)
                                                <a href="{{Helper::url('admin/tin-tuc/edit/'.$key->id)}}">Edit</a> 
                                                @else
                                                <a href="{{Helper::url('admin/dich-vu/edit/'.$key->id)}}">Edit</a> 
                                                @endif
                                                
                                                |
                                            <span><a href="javascript:void(0);" class="text-red remove-item" data-id="{{$key->id}}">Delete</a></span>
                                            
                                        </div>
                                    </td>
                                    <td>
                                        <div style="width:80px; ">
                                            <img src="{{Helper::getImage('media/news',$key->img)}}" alt="{{$key->title}}" title="{{$key->title}}" style="width: 100%;height: auto;">
                                        </div>
                                    </td>
                                    <td>{{date('d/m/y H:i',strtotime($key->created_at))}}</td>
                                    <td class="text-center">
                                        <a class="change-high" href="javascript:void(0);" data-id="{{$key->id}}">
                                        {!!Helper::highLight($key->hightlight)!!}
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a href="javascript:void(0);" class="change-status" data-id="{{$key->id}}">{!!Helper::simpleStatus($key->status)!!}</a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="box-footer">
                    @if($category == 1)
                    <a class="btn btn-primary" href="{{Helper::url('admin/du-an/add')}}">Thêm mới</a>
                    @elseif($category == 2)
                    <a class="btn btn-primary" href="{{Helper::url('admin/tin-tuc/add')}}">Thêm mới</a>
                    @else
                    <a class="btn btn-primary" href="{{Helper::url('admin/dich-vu/add')}}">Thêm mới</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('js_wrapper')
   @include('admin.project.list_js')
@stop
