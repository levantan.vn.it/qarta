<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'State';
    public $timestamps = false;
}
