<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Register</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
    <link rel="stylesheet" href="{{Helper::getThemeCss('admin/bootstrap.min.css')}}"/>
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"/> -->
    <link rel="stylesheet" href="{{Helper::getThemeCss('admin/font-awesome.min.css')}}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"/>
    <link rel="stylesheet" href="{{Helper::getThemeCss('admin/AdminLTE.min.css')}}"/>
    <link rel="stylesheet" href="{{Helper::getThemePlugins('iCheck/square/blue.css')}}"/>
    <link rel="stylesheet" href="{{Helper::getThemeCss('admin/style.css')}}"/>



    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            Register
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">Register a new membership</p>
            {!! Form::open(['url' => Helper::url('register'), 'id' => 'loginForm']) !!}
            <div class="form-group">
                <input type="text" id="email" class="form-control" name="email" placeholder="Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group">
                <input type="text" name="firstname" class="form-control" placeholder="First Name" id="firstname">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="lastname" placeholder="Last Name">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            @if(!empty($country) && count($country))
            <div class="form-group">
                <select class="form-control" id="country" name="country">
                    <option value>Country</option>
                    @foreach($country as $key)
                    <option value="{{$key->id}}">{{$key->Country}}</option>
                    @endforeach
                </select>
            </div>
            @endif
            <div class="form-group">
                <select class="form-control" id="state" name="state">
                    <option value="">State</option>
                </select>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="phone" placeholder="Phone">
                <span class="glyphicon glyphicon-phone-alt form-control-feedback"></span>
            </div>
            <div class="form-group">
                <input type="password" id="password" name="password" class="form-control" placeholder="Passwword">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group">
                <input type="password" name="repassword" id="re-password" class="form-control" placeholder="Retype password">
                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-12" style="text-align: center;">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                </div>
            </div>
            {!! Form::close() !!}
            <a href="{{url('login')}}" style="display: block;padding-top: 10px;">I already have a membership</a>
            <a href="{{url('contact')}}" style="display: block;">Contact us</a>
        </div>
    </div>
    <style type="text/css">
        #loading {z-index: 999999;position: fixed;width: 100%;height: 100%;left: 0;top: 0;background-color: rgba(0, 0, 0, 0.6);}
        .svg-icon-loader {width: 100px;height: 100px;float: left;line-height: 100px;text-align: center;}
        #loading .svg-icon-loader {position: absolute; top: 50%;left: 50%;margin: -50px 0 0 -50px;
        }
        #loading .svg-icon-loader img{
            width: 100px!important
        }
    </style>
    <div id="loading" style="display: none;">
        <div class="svg-icon-loader">
            <img src="{{Helper::getThemeImg('curved-bar.gif')}}" width="200px" alt="">
        </div>
    </div>
    <script src="{{Helper::getThemePlugins('jQuery/jQuery-2.1.4.min.js')}}"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script> -->
    <script src="{{Helper::getThemeJs('admin/bootstrap.min.js')}}"></script>
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> -->
    <script src="{{Helper::getThemePlugins('iCheck/icheck.min.js')}}"></script>
    <script src="{{Helper::getThemeJs('admin/jquery.validate.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#country').on('change',function(){
                var id = $(this).val();
                console.log(id);
                $.ajax({
                    url: "{{url('get_state')}}",
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        id:id,
                        "_token": "{{csrf_token()}}"
                    },
                    beforeSend : function(){
                        $('#loading').show();
                        $('.btn-primary').addClass('disabled');$('.btn-primary').attr('disabled','disabled');
                    },
                    success: function(str) {
                        $('#loading').hide();$('.btn-primary').removeClass('disabled');$('.btn-primary').removeAttr('disabled');
                        if(str.success){
                            $('#state').html(str.data);
                        }
                        return false;
                    }
                });
            });

            $('#loginForm').validate({
                ignore: [],
                rules: {
                    firstname: {
                        required: true,
                        maxlength: 100,
                    },
                    lastname: {
                        required: true,
                        maxlength: 100,
                    },
                    email: {
                        required: true,
                        maxlength: 100,
                        email:true,
                        "remote":
                        {
                            url: '{{url('checkEmail')}}',
                            type: "post",
                            data:
                            {
                                email: function()
                                {
                                    return $('#email').val();
                                },
                                "_token": "{{ csrf_token() }}",
                            }
                        }
                    },
                    phone: {
                        required: true,
                        maxlength: 100,
                        number:true
                    },
                    country: {
                        required: true
                    },
                    state: {
                        required: true
                    },
                    password: {
                        required: true,
                        maxlength: 20,
                        minlength:6
                    },
                    repassword: {
                        required: true,
                        equalTo: '#password'
                    }
                },
                messages: {
                    username:{
                        remote: "Username is already in use"
                    },
                    email:{
                        remote: "Email is already in use"
                    }
                },
                submitHandler: function (form) {
                    var submit_url = $('#loginForm').attr('action');
                    $.ajax({
                        url: submit_url,
                        type: 'POST',
                        dataType: 'JSON',
                        data: $(form).serialize(),
                        beforeSend : function(){
                            $('#loading').show();
                            $('.btn-primary').addClass('disabled');$('.btn-primary').attr('disabled','disabled');
                        },
                        success: function(str) {
                            $('#loading').hide();$('.btn-primary').removeClass('disabled');$('.btn-primary').removeAttr('disabled');
                            $('.form-group').removeClass('has-error');
                            if (!str.success) {
                                $('.login-box-msg').addClass('text-red').text(str.message);
                            } else {
                                $('.login-box-msg').addClass('text-green').text(str.message);
                                window.location.replace('{{  Helper::url("thank-you")  }}');
                            }
                            return false;
                        }
                    });
                    return false;
                }
            });
        });
    </script>

</body>
</html>