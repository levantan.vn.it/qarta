@extends('admin.templates.master')

@section('content')

<section class="content-header">
    <h1>List User</h1>
    <ol class="breadcrumb">
        <li><a href="{{Helper::url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">List User</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">List User</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped table-data">
                        <thead>
                            <tr>
                                <th>FullName</th>
                                <th>Email</th>
                                <th>Country</th>
                                <th>State</th>
                                <th width="100px" class="text-center">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(!empty($user))
                            @foreach ($user as $key)
                                <tr>
                                    <td>{{$key->fullname()}}
                                        <div class="row-actions">
                                            <span class="edit"><a href="javascript:void(0);" data-url="{{Helper::url('user/view/'.$key->id)}}" class="load-modal-content" class="text-green">View</a> | </span>
                                            <span class="edit"><a href="javascript:void(0);" data-url="{{Helper::url('user/edit/'.$key->id)}}" class="load-modal-content">Edit</a> | </span>
                                            <span><a href="javascript:void(0);" class="text-red remove-item" data-id="{{$key->id}}">Delete</a></span>
                                            
                                        </div>
                                    </td>
                                    <td>{{$key->email}}</td>
                                    <td>{{$key->country()->first()->Country}}</td>
                                    <td>{{$key->state()->first()->Region}}</td>
                                    <td class="text-center">
                                        <a href="javascript:void(0);" class="change-status" data-id="{{$key->id}}">{!!Helper::simpleStatus($key->status)!!}</a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
</section>
@stop
@section('js_wrapper')
   @include('admin.user.list_js')
@stop
