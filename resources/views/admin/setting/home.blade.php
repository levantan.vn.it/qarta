@extends('admin.templates.master')

@section('content')

<section class="content-header">
    <h1>Setting page home</h1>
    <ol class="breadcrumb">
        <li><a href="{{Helper::url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Setting page home</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Setting page home</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-info btn-flat add-exclusive" type="button"><i class="fa fa-plus" aria-hidden="true"></i></button>
                    </div>
                </div>
                <div class="box-body">
                @if(!empty($home))
                    @foreach($home as $key)
                    <div class="box box-solid box-acc">
                        <div class="box-header with-border">
                            <h3 class="box-title"></h3>
                        </div>
                        <div class="box-body">
                            <div class="space-exclusive no-remove">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form_row form-group">
                                            <label>En text:</label>
                                            <p class="form_input form-control"/>{{$key->en_title}}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form_row form-group">
                                            <label>Fr text:</label>
                                            <p class="form_input form-control"/>{{$key->fr_title}}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form_row form-group text-center">
                                            <label class="width-100">Action</label>
                                            <a class="btn btn-danger btn-flat" href="javascript:void(0);" data-id="{{$key->id}}"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                            <a class="btn btn-primary btn-flat" href="javascript:void(0);" data-url="{{Helper::url('admin/home/edit/'.$key->id)}}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <img class="img-responsive pad" src="{{Helper::getGallery($key->link)}}" alt="{{$key->name}}" title="{{$key->name}}" >
                        </div>
                    </div>
                    @endforeach
                @endif
                </div>
                <div class="box-body">
                    <div class="form_row form-group">
                        <div class="dropzone media" id="add-media"></div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</section>
<script src="{{Helper::getThemePlugins('dropzone/dist/min/dropzone.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){

    Dropzone.autoDiscover = false;
    var mydropzone = new Dropzone("#add-media",{
        url: "{{ Helper::url('upload_gallery')}}",
        addRemoveLinks : true,
        acceptedFiles: "image/*",
        maxFilesize: 30,
        dictDefaultMessage: '<span class="text-center"><span class="font-lg visible-xs-block visible-sm-block visible-lg-block"><span class="font-lg"><i class="fa fa-cloud-upload" aria-hidden="true"></i> Drag & Drop files here</span><span>&nbsp&nbsp<h4 class="display-inline"> (Or click)</h4></span>',
        dictResponseError: 'Error uploading file!',
        params: {
            _token: "{{ csrf_token() }}",
        },
        dictRemoveFile:'Delete',
        init: function () {
            this.on("removedfile",function(file){
                var id = file.id;
                if(id == null)
                    return false;
                $.ajax({
                    type: "POST",
                    url: "{{Helper::url('removeGallery')}}",
                    data: {
                        id: id,
                        "_token": "{{ csrf_token() }}"
                    },
                    dataType: "JSON",
                    beforeSend : function(){
                    },
                    success: function(result) {
                        return false;
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        modalError(jqXHR.status +' '+errorThrown+'. Please reload and try agian. Thank you!!');
                        // modalError(xhr.responseText);
                    }
                });
            });
            this.on("success",function(file,response){
                $('.dropzone.dz-started .dz-message').css('display','block');
                var data_app = '';
                var scr = $('.dz-image').last().find('img').attr('src');
                data_app = '<div class="fl">'
                                +'<div class="bugdet_attach">'
                                    +'<table width="100%" height="100%">'
                                       +'<tbody>'
                                           +'<tr>'
                                               +'<td align="center">'
                                                +'<img  src="'+scr+'" alt="'+response.data.name+'" title="'+response.data.name+'" class="img-responsive img-b">'
                                               +'</td>'
                                           +'</tr>'
                                       +'</tbody> '
                                    +'</table>'
                                +'</div>'
                                +'<div class="bug-des">'
                                    +'<div>'+response.data.name+'</div>'
                                    +'<div><a href="javascript:void(0);" class="del-gallery" data-id="'+response.data.id+'">Delete</a>'
                                    +'</div> ' 
                                +'</div>'
                            +'</div>';
                $('.att-one').append(data_app);
                $('.dz-success').remove();
            });
        }
    });
});
</script>
@stop
