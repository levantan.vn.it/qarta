@extends('admin.templates.master')

@section('content')

<section class="content-header">
    <h1>Danh mục sản phẩm</h1>
    <ol class="breadcrumb">
        <li><a href="{{Helper::url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Danh mục sản phẩm</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh mục sản phẩm</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped table-data">
                        <thead>
                            <tr>
                                <th>Tên</th>
                                <th>Điện thoại</th>
                                <th>Email</th>
                                <th>Câu hỏi</th>
                                <th>Trả lời</th>
                                <th>Trạng thái</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(!empty($item))
                            @foreach ($item as $key)
                                <tr>
                                    <td>{{$key->name}}
                                        <div class="row-actions">
                                            <span class="edit">
                                                <a href="{{Helper::url('admin/hoi-dap/tra-loi/'.$key->id)}}">Trả lời</a> |
                                            </span>
                                            <span><a href="javascript:void(0);" class="text-red remove-item" data-id="{{$key->id}}">Delete</a></span>
                                            
                                        </div>
                                    </td>
                                    <td>{{$key->phone}}</td>
                                    <td>{{$key->email}}</td>
                                    <td>
                                        {{$key->question}}
                                        
                                    </td>
                                    <td>{{$key->answer}}</td>
                                    <td class="text-center">
                                        <a href="javascript:void(0);" class="change-status" data-id="{{$key->id}}">{!!Helper::simpleStatus($key->status)!!}</a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="box-footer">
                    <a class="btn btn-primary" href="{{Helper::url('admin/hoi-dap/add')}}">Thêm mới</a>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('js_wrapper')
   @include('admin.category.list_js')
@stop
