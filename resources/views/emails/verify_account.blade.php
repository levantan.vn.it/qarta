

<div style="font-size: 16px;">
    <div style="background: #f5f9fa;padding: 20px 0px;">
        <div style="width: 640px;margin: 20px auto; background: #fff;padding: 20px;font-size: 16px;">
            <h2>Email Confirmation</h2>
            <p>Hi {{$firstname}} {{$lastname}}</p>
            <p>Please click on the link below to confirm your account registration</p>
            <p><a href="{{url('verify-account/'.$active_code)}}">Verify your account</a></p>
            <p>VITPR</p>
        </div>
    </div>
</div>