@extends('default::admin.templates.blank')
@section('content')

<div class="">
    <div class="modalTitle">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title text-blue"><i class="fa fa-asterisk"></i>&nbsp;&nbsp;Change Password</h4>
    </div>
    {!! Form::open(['url' => Helper::url('admin/submit_change_password'), 'id' => 'submitFormChange']) !!}
        <div class="box-body">
            <div class="form">
                <div class="form_row form-group">
                    <label>Old Password:<span class="text-red">*</span></label>
                    <input type="password" id="old-password" class="form_input form-control" name="old_password" required value="" />
                </div>
                <div class="form_row form-group">
                    <label>News Password:<span class="text-red">*</span></label>
                    <input type="password" id="new-password" class="form_input form-control" name="password" required value="" />
                </div>
                <div class="form_row form-group">
                    <label>Re Password:<span class="text-red">*</span></label>
                    <input type="password" id="re-password" class="form_input form-control" name="re_password" required value="" />
                </div>
            </div>
        </div>
        <div class="text-center">

            <button type="submit" class="btn btn-success">Submit</button>
        </div>
    {!! Form::close() !!}
</div><!-- /.modal-content -->
<script type="text/javascript">
    $(document).ready(function() {

        $('#submitFormChange').validate({
        ignore: [],
        rules: {
            old_password:{
                required: true,
                maxlength: 20,
                minlength: 6

            },
            name:{
                required: true,
                maxlength: 20,
                minlength: 6
            },
            re_password:{
                required: true,
                maxlength: 20,
                minlength: 6
            }
        },
        messages: {
        },
        submitHandler: function(form) {
            var submit_url = $('#submitFormChange').attr('action');
            $.ajax({
                type: "POST",
                url: submit_url,
                data: $(form).serialize(),
                dataType: "JSON",
                beforeSend : function(){
                    $('#loading').show();
                },
                success: function(result){
                    $('#loading').hide();
                    if(result.success){
                        location.reload();
                        return false;
                    }
                    else{
                        modalError(result.message);
                        return false;
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    $('#loading').hide();
                    modalError(jqXHR.status +' '+errorThrown+'. Please reload and try agian. Thank you!!');
                    // modalError(xhr.responseText);
                }
            });
            return false;
        }
    });
    });
</script>
@stop