
function loadModalContent(id) {
    $('#loading').show();
    var url = id.attr('data-url');
    $.get(url, function(data) {
        $('.modal-body').html(data);
    }).always(function() {
        $('#loading').hide();
    });
    $('.modal').modal('show');
}

function modalDisplay(m_title, m_class, message) {
    new Messi(
        message,
        {
            modal: true,
            modalOpacity: 0.5,
            title: m_title,
            titleClass: m_class
        }
    );
}

function modalError(message) {
    return modalDisplay('Error', 'error', message);
}

function modalWarning(message) {
    return modalDisplay('Warning', 'warning', message);
}

function modalSuccess(message) {
    return modalDisplay('Success', 'success', message);
}

function successPopup(message, link) {
    window.location.replace(link);
    // var dialog = new Messi(
    //     message,
    //     {
    //         modal: true,
    //         modalOpacity: 0.5,
    //         title: 'Success',
    //         titleClass: 'success',
    //         buttons: [
    //             {id: 0, label: 'Yes', val: 'Y'}
    //         ],
    //         callback: function(val) {
    //             window.location.replace(link);
    //         }
    //     }
    // );
}
function loadOptionsList(id, ajax_url, type,arr_tag) {
    $.ajax({
        type: "POST",
        url: ajax_url,
        data: {select : type,
            arr_tag:arr_tag
        },
        dataType: "JSON",
        success: function(result) {
            if (!result.success) {
                modalError(result.message);
            } else {
                $(id).html(result.data);
            }
            return false;
        }
    });
}
function form_select(select_id, placeholder_title, create_url, load_url,arr_tag) {
    var form_content = '<div class="row col-md-12 form-group-color width-form">' +
        '<div class="form-group form-control-wrapper-css has-error">' +
        '<input id="title" name="title" class="form-control form-control-success floating-label" placeholder="' + placeholder_title + '">' +
        '</div>' +
        '</div>';
    window.dialog = new Messi(
        form_content,
        {
            modal: true,
            modalOpacity: 0.5,
            title: 'Please enter your information!',
            titleClass: 'info',
            buttons: [
                {id: 'submit', label: 'Submit', val: 'Submit'},
                {id: 'cancel', label: 'Cancel', val: 'Cancel'}
            ],
            callback: function(val) {
                if (val == 'Submit') {
                    var input_type = $('#title').val();

                    if (input_type == '') {
                        $('.messi-titlebox').removeClass('info').addClass('error');
                    } else {
                        $.ajax({
                            type: "POST",
                            url: create_url,
                            data: {title: input_type},
                            dataType: "JSON",
                            success: function(result) {
                                if (!result.success) {
                                    $('.messi-titlebox').removeClass('info').addClass('error').find('.messi-title').text(result.message);
                                } else {
                                    $('.messi-titlebox').removeClass('info').removeClass('error').addClass('success').find('.messi-title').text(result.message);
                                    loadOptionsList(select_id, load_url, input_type,arr_tag);
                                    dialog.unload();
                                }
                                return false;
                            }
                        });
                    }
                    return false;
                }else{
                    $("#value_other").prop("selected", false);
                    $("#value_other").removeAttr("selected");
                    $('li[title="Other"]').remove();
                }
            }
        }
    );
}



