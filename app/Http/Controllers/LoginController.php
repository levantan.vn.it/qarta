<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 07/10/2016
 * Time: 10:06 SA
 */

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Helper, Mail;
use App\Models\UserModel;
use App\Models\CountryModel;
use App\Models\StateModel;
class LoginController extends Controller
{
    private $userModel;
    public  function  index(){
        $current_user = Helper::getSession('user');
        if(!empty($current_user) && !empty($current_user->id)){
            return redirect('/');
        }
        return view('admin/login');
    }
    public function submit(Request $request)
    {
        $this->userModel = new UserModel;
        // check request from ajax or not
        if (!$request->ajax()) {
            $result = Helper::resultData(FALSE, "Have error");
            return Helper::ajaxOutput($result);
        }

        $data_post = $request->all();
        // dd($data_post);
        // validate post data
        if (empty($data_post) || empty($data_post['email']) || empty($data_post['password']))
        {
            $flag = (empty($data_post) || empty($data_post['email'])) ? 'email' : 'password';
            $result = Helper::resultData(FALSE, "Please enter correct", NULL, $flag);
            return Helper::ajaxOutput($result);
        }

        $checkLogin = $this->userModel->checkLogin($data_post);
        if (!$checkLogin['success']) {
            return Helper::ajaxOutput($checkLogin);
        }


        // set user data to session
        Helper::setSession($checkLogin['message'],'user');

        $result = Helper::resultData(TRUE, "Login success");
        return Helper::ajaxOutput($result);
    }

    public  function  register(){
        $current_user = Helper::getSession('user');
        if(!empty($current_user) && !empty($current_user->id)){
            return redirect('/');
        }
        $this->countryModel = new CountryModel();
        $country = $this->countryModel->getItem();
        return view('admin/register',['country'=>$country]);
    }

    public function get_state(Request $request)
    {
        $this->userModel = new UserModel;
        // check request from ajax or not
        if (!$request->ajax()) {
            $result = Helper::resultData(FALSE, "Have error");
            return Helper::ajaxOutput($result);
        }

        $data_post = $request->all();
        // dd($data_post);
        // validate post data
        $this->stateModel = new StateModel();
        $state = $this->stateModel->getItem(['CountryID'=>$data_post['id']]);
        $html = '<option value="">State</option>';
        if(!empty($state) && count($state)){
            foreach ($state as $key) {
                $html .= '<option value="'.$key->id.'">'.$key->Region.'</option>';
            }
        }

        $result = Helper::resultData(TRUE, "",$html);
        return Helper::ajaxOutput($result);
    }

    public function submit_register(Request $request)
    {
        $this->userModel = new UserModel;
        // check request from ajax or not
        if (!$request->ajax()) {
            $result = Helper::resultData(FALSE, "Have error");
            return Helper::ajaxOutput($result);
        }

        $data_post = $request->all();
        unset($data_post['_token']);
        unset($data_post['repassword']);
        $rules = [
            'firstname' => 'required|max:100',
            'lastname' => 'required|max:100',
            'email' => 'required|email|unique:User,email',
            'country' => 'required',
            'state' => 'required',
            'password' =>'required',
            'phone' => 'required'
        ];
        // run the validation rules on the inputs from the form
        $validator = \Validator::make($data_post, $rules);
        // if the validator fails, return error message
        if($validator->fails()){
            //return validate error message
            $result = Helper::resultData(false, $validator->errors()->first());
            return Helper::ajaxOutput($result);
        }
        $rand = rand(0,100);
        $active_key = sha1(time().time().$data_post['fullname'].$rand);
        $data_post['active_code'] = $active_key;
        $user = $this->userModel->save_item($data_post);
        if(!$user['success']){
            return Helper::ajaxOutput($user);
        }
        $this->countryModel = new CountryModel();
        $this->stateModel = new StateModel();

        $message = view('emails.verify_account',$data_post)->render();
        $subject = 'Verify account';
        Helper::send_mail($data_post['email'],'VITPR',$subject,$message);

        $data_mail['user'] = $user['message'];
        $message = view('emails.new_account',$data_mail)->render();
        $subject = 'New account';
        Helper::send_mail('levantan.vn.it@gmail.com','VITPR',$subject,$message);

        $result = Helper::resultData(TRUE, "Login success");
        return Helper::ajaxOutput($result);
    }

    public function checkEmail(Request $request){
        $data_post = $request->all();
        if(empty($data_post['email'])){
            echo 'false';
            return;
        }
        $email = $data_post['email'];
        $this->userModel = new UserModel;
        $check = $this->userModel->getUser(['email'=>$email]);
        if(!empty($check) && count($check)){
            echo 'false';
            return;
        }
        echo 'true';
        return;
    }

    public function checkUser(Request $request){
        $data_post = $request->all();
        if(empty($data_post['username'])){
            echo 'false';
            return;
        }
        $username = $data_post['username'];
        $this->userModel = new UserModel;
        $check = $this->userModel->getUser(['username'=>$username]);
        if(!empty($check) && count($check)){
            echo 'false';
            return;
        }
        echo 'true';
        return;
    }

    public  function thank_you(){
        $current_user = Helper::getSession('user');
        if(!empty($current_user) && !empty($current_user->id)){
            return redirect('/');
        }
        return view('admin/thank-you');
    }

    public function active_key($key){
        if(empty($key)){
            return redirect('/');
        }
        $this->userModel = new UserModel;
        $check = $this->userModel->getUser(['active_code'=>$key]);
        if(empty($check) || !count($check)){
            return redirect('/');
        }
        $check = $check[0];
        $check->status = 1;
        $check->save();
        return redirect('login');
    }

}