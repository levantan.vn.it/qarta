
$(document).ready(function () {
    $("#bg-slider").owlCarousel({
        navigation: false, // Show next and prev buttons
        slideSpeed: 100,
        autoPlay: 5000,
        paginationSpeed: 100,
        singleItem: true,
        mouseDrag: false,
        transitionStyle: "fade"
    });
    $("#testimonial-slider").owlCarousel({
        navigation: true, // Show next and prev buttons
        slideSpeed: 100,
        pagination: false,
        paginationSpeed: 100,
        items: 3
    });

    $("#other-product-slider").owlCarousel({
        navigation: true, // Show next and prev buttons
        slideSpeed: 100,
        pagination: false,
        paginationSpeed: 100,
        items: 3,
        responsiveClass:true,
        responsive:{
            768:{
                items: 3,
                nav:true
            },
        }
    });

});
