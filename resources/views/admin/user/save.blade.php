@extends('admin.templates.master')

@section('content')
<section class="content-header">
    <h1>{{!empty($item)?'Update':'Created'}}</h1>
    <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('admin/item')}}"> List</a></li>
        <li class="active">{{!empty($item)?'Update':'Created'}}</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">{{!empty($item)?'Update':'Created'}}</h3>
                </div>
                <div class="box-body">
                    <div class="col-xs-12">
                        {!! Form::open(['url' => url('admin/dining/submit_add'), 'id' => 'submitForm']) !!}
                        <div class="form">
                            @if(!empty($item))
                            <input type="hidden" name="id" value="{{$item->id}}">
                            @endif
                            <div class="form_row form-group">
                                <label>Title:</label>
                                <input type="text" class="form_input form-control" name="title" value="{{!empty($item)?$item->title:''}}">
                            </div>
                            <div class="form_row form-group">
                                <label>Description:</label>
                                <textarea rows="4" name="description" class="form_input form-control">{{!empty($item)?$item->description:''}}</textarea>
                            </div>
                            
                            <div class="box box-solid">
                                @if(!empty($item) && !empty($gallery))
                                <div class="box-body">
                                    <table width="100%">
                                        <tbody>
                                            <tr>
                                                <td>
                                                <div class="att-one">
                                                @foreach ($gallery as $key)
                                                    <div class="fl">
                                                        <div class="bugdet_attach">
                                                            <table width="100%" height="100%">
                                                               <tbody>
                                                                   <tr>
                                                                       <td align="center">
                                                                        <img  src="{{Helper::getGallery($key->link)}}" alt="{{$key->name}}" title="{{$key->name}}" class="img-responsive img-b">
                                                                       </td>
                                                                   </tr>
                                                               </tbody> 
                                                            </table>
                                                        </div>
                                                        <div class="bug-des">
                                                            <div>{{$key->name}}</div>
                                                            <div><a href="javascript:void(0);" class="del-gallery" data-id="{{$key->id}}">Delete</a>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                @endforeach
                                                </div>
                                                </td>
                                            </tr>
                                        </tbody>  
                                    </table>
                                </div>
                                @endif
                                <div class="box-body">
                                    <div class="form_row form-group">
                                        <div class="dropzone media" id="add-media"></div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="{{url('assets/plugins/dropzone/dist/min/dropzone.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
    
    $('#submitForm').validate({
        ignore: [],
        rules: {
            title:{
                maxlength: 100
            },
        },
        messages: {
        },
        submitHandler: function(form) {
            var submit_url = $('#submitForm').attr('action');
            $.ajax({
                type: "POST",
                url: submit_url,
                data: $(form).serialize(),
                dataType: "JSON",
                beforeSend : function(){
                    $('#loading').show();
                },
                success: function(result){
                    $('#loading').hide();
                    if(result.success){
                        location.replace("{{url('admin/dining')}}");
                        return false;
                    }
                    else{
                        modalError(result.message);
                        return false;
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    $('#loading').hide();
                    modalError(jqXHR.status +' '+errorThrown+'. Please reload and try agian. Thank you!!');
                    // modalError(xhr.responseText);
                }
            });
            return false;
        }
    });

    Dropzone.autoDiscover = false;
    var mydropzone = new Dropzone("#add-media",{
        url: "{{url('upload_tmp')}}",
        addRemoveLinks : true,
        acceptedFiles: "image/*",
        maxFilesize: 30,
        dictDefaultMessage: '<span class="text-center"><span class="font-lg visible-xs-block visible-sm-block visible-lg-block"><span class="font-lg"><i class="fa fa-cloud-upload" aria-hidden="true"></i> Drag & Drop files here</span><span>&nbsp&nbsp<h4 class="display-inline"> (Or click)</h4></span>',
        dictResponseError: 'Error uploading file!',
        params: {
            _token: "{{ csrf_token() }}",
        },
        dictRemoveFile:'Delete',
        init: function () {
            this.on("removedfile",function(file){
                var name = file.id;
                if(name == null)
                    return false;
                $("input[name='gallery["+name+"]']").remove();
                $.ajax({
                    type: "POST",
                    url: "{{Helper::url('remove_tmp')}}",
                    data: {
                        name: name,
                        "_token": "{{ csrf_token() }}"
                    },
                    dataType: "JSON",
                    beforeSend : function(){
                    },
                    success: function(result) {
                        return false;
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        modalError(jqXHR.status +' '+errorThrown+'. Please reload and try agian. Thank you!!');
                        // modalError(xhr.responseText);
                    }
                });
            });
            this.on("success",function(file,response){
                $("#add-media").append("<input type='hidden' id='"+response.data+"' value='"+response.flag+"' name='gallery["+response.data+"]'/>");
                file.id  = response.data;
            });
        }
    });
});
</script>
@stop
