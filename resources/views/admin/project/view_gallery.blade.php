@extends('admin.templates.master')

@section('content')

<section class="content-header">
    <h1>Gallery Dining</h1>
    <ol class="breadcrumb">
        <li><a href="{{Helper::url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{Helper::url('admin/dining')}}"> Dining</a></li>
        <li class="active">Gallery Dining</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Gallery: {{$dining->title}}</h3>
                </div>
                <div class="box-body">
                    <table width="100%">
                        <tbody>
                            <tr>
                                <td>
                                <div class="att-one">
                                @if(!empty($gallery))
                                    @foreach ($gallery as $key)
                                    <div class="fl">
                                        <div class="bugdet_attach">
                                            <table width="100%" height="100%">
                                               <tbody>
                                                   <tr>
                                                       <td align="center">
                                                        <img  src="{{Helper::getGallery($key->link)}}" alt="{{$key->name}}" title="{{$key->name}}" class="img-responsive img-b">
                                                       </td>
                                                   </tr>
                                               </tbody> 
                                            </table>
                                        </div>
                                        <div class="bug-des">
                                            <div>{{$key->name}}</div>
                                            <div><a href="javascript:void(0);" class="del-gallery" data-id="{{$key->id}}">Delete</a>

                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                @endif
                                </div>
                                </td>
                            </tr>
                        </tbody>  
                    </table>
                </div>
                <div class="box-body">
                    <div class="form_row form-group">
                        <div class="dropzone media" id="add-media"></div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</section>
<script src="{{Helper::getThemePlugins('dropzone/dist/min/dropzone.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){


    Dropzone.autoDiscover = false;
    var mydropzone = new Dropzone("#add-media",{
        url: "{{ Helper::url('upload_gallery')}}",
        addRemoveLinks : true,
        acceptedFiles: "image/*",
        maxFilesize: 30,
        dictDefaultMessage: '<span class="text-center"><span class="font-lg visible-xs-block visible-sm-block visible-lg-block"><span class="font-lg"><i class="fa fa-cloud-upload" aria-hidden="true"></i> Drag & Drop files here</span><span>&nbsp&nbsp<h4 class="display-inline"> (Or click)</h4></span>',
        dictResponseError: 'Error uploading file!',
        params: {
            _token: "{{ csrf_token() }}",
            page_id: "{{$dining->id}}",
            type_page: 2
        },
        dictRemoveFile:'Delete',
        init: function () {
            this.on("removedfile",function(file){
                var id = file.id;
                if(id == null)
                    return false;
                $.ajax({
                    type: "POST",
                    url: "{{Helper::url('removeGallery')}}",
                    data: {
                        id: id,
                        "_token": "{{ csrf_token() }}"
                    },
                    dataType: "JSON",
                    beforeSend : function(){
                    },
                    success: function(result) {
                        return false;
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        modalError(jqXHR.status +' '+errorThrown+'. Please reload and try agian. Thank you!!');
                        // modalError(xhr.responseText);
                    }
                });
            });
            this.on("success",function(file,response){
                $('.dropzone.dz-started .dz-message').css('display','block');
                var data_app = '';
                var scr = $('.dz-image').last().find('img').attr('src');
                data_app = '<div class="fl">'
                                +'<div class="bugdet_attach">'
                                    +'<table width="100%" height="100%">'
                                       +'<tbody>'
                                           +'<tr>'
                                               +'<td align="center">'
                                                +'<img  src="'+response.data.link+'" alt="'+response.data.name+'" title="'+response.data.name+'" class="img-responsive img-b">'
                                               +'</td>'
                                           +'</tr>'
                                       +'</tbody> '
                                    +'</table>'
                                +'</div>'
                                +'<div class="bug-des">'
                                    +'<div>'+response.data.name+'</div>'
                                    +'<div><a href="javascript:void(0);" class="del-gallery" data-id="'+response.data.id+'">Delete</a>'
                                    +'</div> '
                                +'</div>'
                            +'</div>';
                $('.att-one').append(data_app);
                $('.dz-success').remove();
            });
        }
    });
});
</script>
@stop
