<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 07/10/2016
 * Time: 10:06 SA
 */

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Helper, Input, File, Mail;
use App\Models\UserModel;
use App\Models\CountryModel;
use App\Models\StateModel;
class AdminController extends Controller
{
    private $userModel;
    private $countryModel;
    private $stateModel;

    public  function index(){
        $this->userModel = new UserModel();
        
        $current_user = Helper::getSession('user');
        if(empty($current_user)){
            return redirect('login');
        }
        $user_active = $this->userModel->getUser(['status'=>1]);
        $user_deactive = $this->userModel->getUser(['status'=>2]);
        $user_unverified = $this->userModel->getUser(['status'=>0]);
        $user = $this->userModel->findUser($current_user->id);
        $data = [
            'current_user'  =>  $current_user,
            'user_active' =>  $user_active,
            'user_deactive' =>  $user_deactive,
            'user_unverified'   =>  $user_unverified,
            'user'  =>  $user
        ];
        return view('admin/home',$data);
    }
    public function logout()
    {
        session()->flush();
        return redirect('/login');

    }

    public  function change_password(){
        $current_user = Helper::getSession('user');
        if(empty($current_user)){
            return redirect('login');
        }
        $data = [
            'current_user'  =>  $current_user
        ];
        return view('admin.setting.change_password',$data);
    }

    public function submit_change_password(Request $request){
        // check request from ajax or not
        if (!$request->ajax()) {
            $result = Helper::resultData(FALSE, "Dont hit me");
            return Helper::ajaxOutput($result);
        }

        $current_user = Helper::getSession('user');
        if(empty($current_user) || empty($current_user->id)){
            $result = Helper::resultData(FALSE,"Dont hit me");
            return Helper::ajaxOutput($result);
        }
        $data_post = $request->all();
        unset($data_post['_token']);
        $this->userModel = new UserModel();
        $result = $this->userModel->edit_password($data_post);
        return Helper::ajaxOutput($result);
    }

    public function user(){
        $this->userModel = new UserModel();
        
        $current_user = Helper::getSession('user');
        if(empty($current_user)){
            return redirect('login');
        }
        $user = $this->userModel->getUser(['group'=>2]);
        
        $data = [
            'current_user'  =>  $current_user,
            'user' =>  $user,
        ];
        return view('admin/user/list',$data);
    }


    public function updateStatus(Request $request){
        $this->userModel = new UserModel();
        // check request from ajax or not
        if (!$request->ajax()) {
            $result = Helper::resultData(FALSE, "Dont hit me");
            return Helper::ajaxOutput($result);
        }

        $current_user = Helper::getSession('user');
        if(empty($current_user) || empty($current_user->id)){
            return redirect('/admin/login');
        }
        $data_post = $request->all();
        // var_dump($data_post);die();
        if(empty($data_post['_token'])){
            $result = Helper::resultData(FALSE, "Dont hit me");
            return Helper::ajaxOutput($result);
        }
        if(empty($data_post['id'])){
            $result = Helper::resultData(FALSE, "User not found");
            return Helper::ajaxOutput($result);
        }
        unset($data_post['_token']);

        $item = $this->userModel->findUser($data_post['id']);
        // if post not exist
        if (!count($item)) {
            $result = Helper::resultData(FALSE, "User not found");
            return Helper::ajaxOutput($result);
        }
        // update status
        $action = $this->userModel->updateStatus($item);
        // if have error when update
        if (!$action) {
            $result = Helper::resultData(FALSE, "Dont hit me");
            return Helper::ajaxOutput($result);
        }

        $result = Helper::resultData(TRUE, "",$action);
        return Helper::ajaxOutput($result);
    }

    public function removeUser(Request $request){
        $this->userModel = new UserModel();
        // check request from ajax or not
        if (!$request->ajax()) {
            $result = Helper::resultData(FALSE, "Dont hit me");
            return Helper::ajaxOutput($result);
        }

        $current_user = Helper::getSession('user');
        if(empty($current_user) || empty($current_user->id)){
            return redirect('/login');
        }
        $data_post = $request->all();
        // var_dump($data_post);die();
        if(empty($data_post['_token'])){
            $result = Helper::resultData(FALSE, "Dont hit me");
            return Helper::ajaxOutput($result);
        }
        if(empty($data_post['id'])){
            $result = Helper::resultData(FALSE, "User not found");
            return Helper::ajaxOutput($result);
        }
        unset($data_post['_token']);

        $item = $this->userModel->removeItem($data_post['id']);

        // if post not exist
        if (!$item) {
            $result = Helper::resultData(FALSE, "User not found");
            return Helper::ajaxOutput($result);
        }
        $result = Helper::resultData(TRUE);
        return Helper::ajaxOutput($result);
    }

    public function view_user($id){
        $this->userModel = new UserModel();
        
        $current_user = Helper::getSession('user');
        if(empty($current_user)){
            return redirect('login');
        }
        if(empty($id)){
            echo "User not found";return;
        }
        $user = $this->userModel->findUser($id);
        if(empty($user)){
            echo "User not found";return;
        }
        $data = [
            'current_user'  =>  $current_user,
            'user' =>  $user,
        ];
        return view('admin/user/view',$data);
    }

    public function edit_user($id){
        $this->userModel = new UserModel();
        
        $current_user = Helper::getSession('user');
        if(empty($current_user)){
            return redirect('login');
        }
        if(empty($id)){
            echo "User not found";return;
        }
        $user = $this->userModel->findUser($id);
        if(empty($user)){
            echo "User not found";return;
        }
        $this->countryModel = new CountryModel();
        $this->stateModel = new StateModel();
        $country = $this->countryModel->getItem();
        $state = $this->stateModel->getItem(['CountryID'=>$user->country]);
        $data = [
            'current_user'  =>  $current_user,
            'user' =>  $user,
            'country'   =>  $country,
            'state' =>  $state
        ];
        return view('admin/user/edit',$data);
    }

    public function submit_edit_user(Request $request)
    {
        $this->userModel = new UserModel;
        // check request from ajax or not
        if (!$request->ajax()) {
            $result = Helper::resultData(FALSE, "Have error");
            return Helper::ajaxOutput($result);
        }

        $data_post = $request->all();
        unset($data_post['_token']);
        unset($data_post['repassword']);
        $rules = [
            'firstname' => 'required|max:100',
            'lastname' => 'required|max:100',
            'email' => 'required|email',
            'country' => 'required',
            'state' => 'required',
            'phone' => 'required'
        ];
        // run the validation rules on the inputs from the form
        $validator = \Validator::make($data_post, $rules);
        // if the validator fails, return error message
        if($validator->fails()){
            //return validate error message
            $result = Helper::resultData(false, $validator->errors()->first());
            return Helper::ajaxOutput($result);
        }

        $user = $this->userModel->save_item($data_post);
        if(!$user['success']){
            return Helper::ajaxOutput($user);
        }
        $result = Helper::resultData(TRUE, "Login success");
        return Helper::ajaxOutput($result);
    }

    public  function sendmail(){
        $this->userModel = new UserModel();
        
        $current_user = Helper::getSession('user');
        if(empty($current_user)){
            return redirect('login');
        }
        $data = [
            'fullname'  =>  $current_user->fullname,
            'email' =>  $current_user->email,
            'active_code'   =>  $current_user->active_code
        ];
        Mail::send('emails.verify_account', $data, function ($message) use ($data) {
            $message->from('levantan.vn.it@gmail.com', 'VITPR');
            $message->to($data['email'])->subject('Verify account');

        });
        return redirect('');
    }

    public  function contact(){
        $this->userModel = new UserModel();
        
        $current_user = Helper::getSession('user');
        if(empty($current_user)){
            return view('admin/user/contact_none');
        }
        $data = [
            'current_user'  =>  $current_user,
        ];
        return view('admin/user/contact',$data);
    }


    public function submit_contact(Request $request)
    {
        $this->userModel = new UserModel;
        // check request from ajax or not
        if (!$request->ajax()) {
            $result = Helper::resultData(FALSE, "Have error");
            return Helper::ajaxOutput($result);
        }
        $data_post = $request->all();
        $current_user = Helper::getSession('user');
        if(empty($current_user)){
            // Mail::send('emails.contact_none', $data_post, function ($message) use ($data_post) {
            //     $message->from('levantan.vn.it@gmail.com', 'VITPR');
            //     $message->to('levantan.vn.it@gmail.com')->subject($data_post['subject']);
            // });

            $message = view('emails.contact_none',$data_post)->render();
            $subject = $data_post['subject'];
            Helper::send_mail('levantan.vn.it@gmail.com',$data_post['fullname'],$subject,$message);


            // Mail::send('emails.re-contact', $data_post, function ($message) use ($data_post) {
            //     $message->from('levantan.vn.it@gmail.com', 'Reply - ViTPR');
            //     $message->to($data_post['email'])->subject('RE: New message from ViTPR');

            // });

            $message = view('emails.re-contact',$data_post)->render();
            $subject = "RE: New message from ViTPR";
            Helper::send_mail($data_post['email'],'Reply - ViTPR',$subject,$message);

            $result = Helper::resultData(TRUE);
            return Helper::ajaxOutput($result);
        }

        
        $data = [
            'current_user'  =>  $current_user,
            'data_post' =>  $data_post,
        ];
        Mail::send('emails.contact', $data, function ($message) use ($data) {
            $message->from('levantan.vn.it@gmail.com', 'VITPR');
            $message->to('levantan.vn.it@gmail.com')->subject($data['current_user']->fullname.' send contact');
        });
        $result = Helper::resultData(TRUE);
        return Helper::ajaxOutput($result);
    }



}