
<script>
    var base_url = admin_url+"/du-an";
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });


     // change post status
    $('.change-status').on('click', function() {
        var obj = $(this);
        var id = obj.attr('data-id');
        $.ajax({
            type: "POST",
            url: base_url+'/updateStatus',
            data: {
                id: id,
                "_token": "{{ csrf_token() }}"
            },
            dataType: "JSON",
            beforeSend : function(){
                $('#loading').show();
            },
            success: function(result) {
                $('#loading').hide();
                if (result.success) {
                    if (result.data == 2) {
                        obj.find('span').removeClass('label-success').addClass('label-danger').text('UnActive');
                    } else {
                        obj.find('span').removeClass('label-danger').addClass('label-success').text('Active');
                    }
                }
                return false;
            },
            error: function(jqXHR, textStatus, errorThrown){
                $('#loading').hide();
                modalError(jqXHR.status +' '+errorThrown+'. Please reload and try agian. Thank you!!');
                // modalError(xhr.responseText);
            }
        });
        return false;
    });

    $('.remove-item').on('click', function() {
        var obj = $(this);
        var id = obj.attr('data-id');
        var dialog = new Messi(
            "Do you want remove this",
            {
                modal: true,
                modalOpacity: 0.5,
                title: 'Confirmation',
                titleClass: 'warning',
                buttons: [
                    {id: 0, label: 'Yes', val: 'Y'},
                    {id: 1, label: 'No', val: 'N'}
                ],
                callback: function(val) {
                    if (val == 'Y') {
                        $.ajax({
                            type: "POST",
                            url:  base_url+'removeItem',
                            data: {
                                id: id,
                                "_token": "{{ csrf_token() }}"
                            },
                            dataType: "JSON",
                            beforeSend : function(){
                                $('#loading').show();
                            },
                            success: function(result) {
                                $('#loading').hide();
                                if (result.success){
                                    location.reload();
                                }else{
                                    modalError(result.message);
                                }
                                return false;
                            },
                            error: function(jqXHR, textStatus, errorThrown){
                                $('#loading').hide();
                                modalError(jqXHR.status +' '+errorThrown+'. Please reload and try agian. Thank you!!');
                                // modalError(xhr.responseText);
                            }
                        });
                    }
                }
            }
        );
        return false;
    });

    $('.change-high').on('click', function() {
        var obj = $(this);
        var id = obj.attr('data-id');
        $.ajax({
            type: "POST",
            url:  base_url+"/updateHigh",
            data: {
                id: id,
                "_token": "{{ csrf_token() }}"
            },
            dataType: "JSON",
            beforeSend : function(){
                $('#loading').show();
            },
            success: function(result) {
                $('#loading').hide();$('.btn-success').removeClass('disabled');
                if (result.success) {
                    if (result.data == 2) {
                        obj.find('span').removeClass('label-success').addClass('label-danger').text('No');
                    } else {
                        obj.find('span').removeClass('label-danger').addClass('label-success').text('Yes');
                    }
                }
                return false;
            },
            error: function(jqXHR, textStatus, errorThrown){
                $('#loading').hide();
                modalError(jqXHR.status +' '+errorThrown+'. Please reload and try agian. Thank you!!');
            }
        });
        return false;
    });

    $(".table-data").DataTable({
        "pagingType": "full_numbers",
        "ordering": false,
    });

</script>