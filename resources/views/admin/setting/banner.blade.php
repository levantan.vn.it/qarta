@extends('admin.templates.master')

@section('content')
<section class="content-header">
    <h1>Banner quảng cáo</h1>
    <ol class="breadcrumb">
        <li><a href="{{Helper::url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Banner quảng cáo</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Banner quảng cáo</h3>
                </div>
                {!! Form::open(['url' => Helper::url('admin/cai-dat/submit_banner'), 'id' => 'submitForm']) !!}
                <div class="box-body">
                    <div class="box box-solid">
                        <div class="box-body">
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <td>
                                        <div class="att-one">
                                        @if(!empty($gallery))
                                            @foreach ($gallery as $key)
                                            
                                            <div class="fl">
                                                <div class="bugdet_attach">
                                                    <table width="100%" height="100%">
                                                       <tbody>
                                                           <tr>
                                                               <td align="center">
                                                                <img  src="{{Helper::getGallery($key->link)}}" alt="{{$key->name}}" title="{{$key->name}}" class="img-responsive img-b">
                                                               </td>
                                                           </tr>
                                                       </tbody> 
                                                    </table>
                                                </div>
                                                <div class="bug-des">
                                                    <div>{{$key->name}}</div>
                                                    <div><a href="javascript:void(0);" class="del-gallery" data-id="{{$key->id}}">Delete</a>
                                                    </div>  
                                                </div>
                                            </div>
                                            @endforeach
                                        @endif
                                        </div>
                                        </td>
                                    </tr>
                                </tbody>  
                            </table>
                        </div>
                        <div class="box-body">
                            <div class="form_row form-group">
                                <div class="dropzone media" id="add-media"></div>
                            </div> 
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
<script src="{{Helper::getThemePlugins('dropzone/dist/min/dropzone.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){

    $('#submitForm').validate({
        ignore: [],
        rules: {
        },
        messages: {
        },
        submitHandler: function(form) {
            var submit_url = $('#submitForm').attr('action');
            $.ajax({
                type: "POST",
                url: submit_url,
                data: $(form).serialize(),
                dataType: "JSON",
                beforeSend : function(){
                    $('#loading').show();
                },
                success: function(result){
                    $('#loading').hide();
                    if(result.success){
                        location.reload();
                        return false;
                    }
                    else{
                        modalError(result.message);
                        return false;
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    $('#loading').hide();
                    modalError(jqXHR.status +' '+errorThrown+'. Please reload and try agian. Thank you!!');
                    // modalError(xhr.responseText);
                }
            });
            return false;
        }
    });

    Dropzone.autoDiscover = false;
    var mydropzone = new Dropzone("#add-media",{
        url: "{{ Helper::url('admin/upload_tmp')}}",
        addRemoveLinks : true,
        acceptedFiles: "image/*",
        maxFilesize: 30,
        dictDefaultMessage: '<span class="text-center"><span class="font-lg visible-xs-block visible-sm-block visible-lg-block"><span class="font-lg"><i class="fa fa-cloud-upload" aria-hidden="true"></i> Drag & Drop files here</span><span>&nbsp&nbsp<h4 class="display-inline"> (Or click)</h4></span>',
        dictResponseError: 'Error uploading file!',
        params: {
            _token: "{{ csrf_token() }}",
        },
        dictRemoveFile:'Delete',
        init: function () {
            this.on("removedfile",function(file){
                var name = file.id;
                if(name == null)
                    return false;
                $("input[name='gallery["+name+"]']").remove();
                $.ajax({
                    type: "POST",
                    url: "{{Helper::url('admin/remove_tmp')}}",
                    data: {
                        name: name,
                        "_token": "{{ csrf_token() }}"
                    },
                    dataType: "JSON",
                    beforeSend : function(){
                    },
                    success: function(result) {
                        return false;
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        modalError(jqXHR.status +' '+errorThrown+'. Please reload and try agian. Thank you!!');
                        // modalError(xhr.responseText);
                    }
                });
            });
            this.on("success",function(file,response){

                $("#add-media").append("<input type='hidden' id='"+response.data+"' value='"+response.flag+"' name='gallery["+response.data+"]'/>");
                file.id  = response.data;
            });
            this.on("maxfilesexceeded", function(file){
                alert("No more files please!");
                $('.dz-error').remove();
            });
        }
    });
});
</script>
@stop
