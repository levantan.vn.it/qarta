<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'User';
    public $timestamps = true;
    public function country()
    {
        return $this->belongsTo(new Country(), 'country', 'id');
    }

    public function state()
    {
        return $this->belongsTo(new State(), 'state', 'id');
    }

    public function fullname()
    {
        return $this->firstname.' '.$this->lastname;
    }

}
