// slider setting
$('#main-slider').slick({
  dots: false,
  autoplay: true,
  autoplaySpeed: 6000,
  speed:700,
  fade:false,
  pauseOnHover:false,
  mobileFirst: true,
});

// Close menu when click
$(document).on('click',function(){
	$('.collapse').collapse('hide');
});

$(window).load(function() {
	// Animate loader off screen		
	setInterval(function(){ $(".se-pre-con").fadeOut("slow"); }, 1500);
	$(".se-pre-con").css("display","none");
});

$(document).ready(function(){

	if($(window).scrollTop()>50)
	{
		$("#section-main-menu").removeClass("section-main-menu-fixed");
	}

  	// slider setting
  	$('#main-slider').slick({
      dots: false,
      autoplay: true,
      autoplaySpeed: 6000,
      speed:1800,
      fade:true,
      pauseOnHover:false,
      mobileFirst: true,
    });

  	// fancy box setting
	$(".fancybox").fancybox({
		prevEffect		: 'fade',
		nextEffect		: 'fade',
		prevSpeed		: 600,
		nextSpeed		: 600
	});

	// scroll when click menu
	$(document).on('click', 'a[href^="#"]', function(e) {
	    // target element id
	    var id = $(this).attr('href');

	    // target element
	    var $id = $(id);
	    if ($id.length === 0) {
	        return;
	    }

	    // prevent standard hash navigation (avoid blinking in IE)
	    e.preventDefault();

	    // top position relative to the document
	    var pos = $(id).offset().top-58;

	    // animated top scrolling
	    $('body, html').animate({scrollTop: pos});
	});

	// slider height after load
	$("#main-slider").css("height",$(window).height());

	// hidden or show fixed footer after load
	if($(window).scrollTop()+$(window).height()>($(document).height()-$("#section-find-us").height()))
	{
		$(".section-footer-hidden").css("display","none");

	}
	else{
    	$(".section-footer-hidden").css("display","block");
	}


});

$(window).resize(function(){
	// slider height when resize window
	$("#main-slider").css("height",$(window).height());
});

/*

$(window).scroll(function(){

	// hidden or show menu when scroll
	if($(window).scrollTop()>50)
	{
		$("#section-main-menu").addClass("section-main-menu-fixed");
	}
	else
	{
		$("#section-main-menu").removeClass("section-main-menu-fixed");
	}


	// hidden or show fixed footer when scroll
	if($(window).scrollTop()+$(window).height()>($(document).height()-$("#section-find-us").height()))
	{
		$(".section-footer-hidden").css("display","none");

	}
	else{
  		$(".section-footer-hidden").css("display","block");
	}
});*/
