<?php
$current_user = Helper::getSession('user');
?>

<aside class="main-sidebar">
    <section class="sidebar" style="padding-bottom: 50px">

        <ul class="sidebar-menu">
            <li class="header"></li>
            <li class="{{ Helper::activeSegments(1, '') }}">
                <a href="{{ Helper::url('/') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            @if($current_user->group == 1)
            <li class="treeview {{ Helper::activeSegments(1, 'user') }}">
                <a href="#">
                    <i class="fa fa-circle-o"></i> <span>User</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                
                <ul class="treeview-menu">
                    <li class="{{ Helper::activeRoutes('user') }}">
                        <a href="{{ Helper::url('user') }}">
                            <i class="fa fa-circle-o"></i> List
                        </a>
                    </li>
                </ul>
            </li>
            @else
            <li class="treeview {{ Helper::activeSegments(1, 'contact') }}">
                <a href="{{url('contact')}}">
                    <i class="fa fa-circle-o"></i> <span>Contact</span>
                </a>
            </li>
            @endif
        </ul>
    </section>
</aside>
