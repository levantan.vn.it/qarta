<?php

namespace App\Models;
use App\Entities\State;
use Validator, Helper;
class StateModel
{
    /**
     * check user login
     * @param $data
     * @return bool
     */
    public function getItem($where = FALSE, $limit = FALSE,$order_by = FALSE)
    {
        $item = new State();
        if (is_array($limit)) {
            $item = $item->limit($limit[0])->offset($limit[1]);
        } elseif ($limit) {
            $item = $item->limit($limit);
        }else{
            $item = $item;
        }
        if($where){
            $item = $item->where($where);
        }
        if($order_by && is_array($order_by)){
            $item = $item->orderBy($order_by[0],$order_by[1]);
        }else{
            $item = $item->orderBy('id','DESC');
        }
        return $item->get();
    }
    /**
     * [add_item them item vao he thong]
     * @param [array] $data
     */
    public function save_item($data){
        if(!empty($data['id'])){
            $old_item = $this->findItem($data['id']);
            if(empty($old_item)){
                $result = Helper::resultData(false, "item not found");
                return $result;
            }
            $item = Helper::mapData($old_item,$data);
        }else{
            $item = Helper::mapData(new State(),$data);
        }
        
        $result = $item->save();
        if($result){
            $result = Helper::resultData(true,$item);
            return $result;
        }
        $result = Helper::resultData(false, "Don't hit me");
        return $result;
    }

    /**
     * [finditem tim item by id]
     * @param  [type] $id [ma item]
     * @return [mixed]
     */
    public function findItem($id){
        $item = State::find($id);
        return $item;
    }
    /**
     * update item status
     * @param $item
     * @return bool|int
     */
    public function updateStatus($item)
    {
        $status = $item->status == 2 ? 1 : 2;

        $item->status = $status;
        $result = $item->save();
        if ($result) {
            return $status;
        }
        return FALSE;
    }


    public function removeItem($ids){
        return State::destroy($ids);
    }
}
