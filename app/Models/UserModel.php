<?php

namespace App\Models;
use App\Entities\User;
use App\Entities\VisitsPage;
use Helper,Validator;
class UserModel
{
    /**
     * check user login
     * @param $data
     * @return bool
     */
    public function checkLogin($data)
    {

        $user = User::where('email',$data['email'])->first();

        // check user is exist
        if (!count($user)) {
            return Helper::resultData(FALSE,"Email not exist");

        }

        // check passwords are match
        if (!password_verify($data['password'], $user->password)) {
            return Helper::resultData(FALSE,"Password not correct");
        }

        if($user->status == 2){
            return Helper::resultData(FALSE,"Your account has been locked. Please contact admin for more details");
        }

        // unset secure information
        $user->password = NULL;

        return Helper::resultData(TRUE,$user);
    }
    /**
     * get user data by email
     * @param $email
     * @return mixed
     */
    public function getUserByAccount($account, $fields = [])
    {
        $user = User::where('username', $account);
        if (!empty($fields)) {
            $user->select($fields);
        }

        return $user->first();
    }

    public function findUser($id)
    {
        $user = User::find($id);

        return $user;
    }
    /**
     * encode password by hash and salt
     * @param $password
     * @return bool|string
     */
    public function generatePassword($password)
    {
        $options = [
            'cost' => 12,
            'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM)
        ];
        return password_hash($password, PASSWORD_BCRYPT, $options);
    }

    public function edit_account($data){
        $old_item = User::find($data['id']);
        $item = Helper::mapData($old_item,$data);
        $result = $item->save();
        if($result){
            Helper::setSession($item,'user');
            return Helper::resultData(TRUE);
        }
        return Helper::resultData(FALSE,"Change error. Please try agian.");
    }

    public function edit_password($data){
        $user = User::find(1);
        // check passwords are match
        if (!password_verify($data['old_password'], $user->password)) {
            return Helper::resultData(FALSE,"Password not correct");
        }
        $user->password = $this->generatePassword($data['password']);
        $result = $user->save();
        if($result){
            return Helper::resultData(TRUE);
        }
        return Helper::resultData(FALSE,"Change error. Please try agian.");
    }

    public function getUser($where = FALSE, $limit = FALSE,$order_by = FALSE)
    {
        $visit = new User();
        if (is_array($limit)) {
            $visit = $visit->limit($limit[0])->offset($limit[1]);
        } elseif ($limit) {
            $visit = $visit->limit($limit);
        }else{
            $visit = $visit;
        }
        if($where){
            $visit = $visit->where($where);
        }
        
        if($order_by && is_array($order_by)){
            $visit = $visit->orderBy($order_by[0],$order_by[1]);
        }else{
            $visit = $visit->orderBy('id','DESC');
        }
        return $visit->get();
    }
    public function checkLocation($month = FALSE,$year = FALSE)
    {
        $report = new VisitsPage();
        if($month){
            $report = $report->whereMonth('created_time','=',$month);
        }
        if($year){
            $report = $report->whereYear('created_time','=',$year);
        }
        $report = $report->groupBy('ip_address');
        $reports = $report->get();
        return $reports;
    }
    /**
     * [add_item them item vao he thong]
     * @param [array] $data
     */
    public function save_item($data){
        if(!empty($data['id'])){
            $old_item = User::find($data['id']);
            if(empty($old_item)){
                $result = Helper::resultData(false, "User not found");
                return $result;
            }
            $item = Helper::mapData($old_item,$data);
        }else{
            if(!empty($data['password'])){
                $data['password']   =   $this->generatePassword($data['password']);
            }
            $item = Helper::mapData(new User(),$data);
        }
        
        $result = $item->save();
        if($result){
            $result = Helper::resultData(true,$item);
            return $result;
        }
        $result = Helper::resultData(false, "Don't hit me");
        return $result;
    }

    /**
     * update setting status
     * @param $setting
     * @return bool|int
     */
    public function updateStatus($setting)
    {
        $status = $setting->status == 2 ? 1 : 2;

        $setting->status = $status;
        $result = $setting->save();

        if ($result) {
            return $status;
        }
        return FALSE;
    }

    public function removeItem($ids){
        return User::destroy($ids);
    }
}
