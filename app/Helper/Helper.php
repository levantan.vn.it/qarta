<?php

namespace App\Helper;
use App\Entities\Gallery;
use App\Entities\Setting;
use App\Entities\Faq;
use App\Entities\Category;
use Illuminate\Support\Facades\Route;
use PHPMailerAutoload;
use PHPMailer; 
class Helper
{
    public function currentDomain($all = false, $suffix = true)
    {
        if (empty($_SERVER["SERVER_NAME"])) {
            return false;
        }

        $currentDomain = str_replace('www.', '', $_SERVER["SERVER_NAME"]);
        if ($all) {
            return $currentDomain;
        }

        $exDomain = explode('.', $currentDomain);
        $total = count($exDomain);
        $domain = $exDomain[$total - 2];

        if ($suffix) {
            $domain .= '.'.end($exDomain);
        }

        return $domain;
    }

    /**
     * show result data in array
     * @param bool|false $success
     * @param string $message
     * @param null $data
     * @param int $flag
     * @return array
     */
    public function resultData($success = FALSE, $message = '', $data = NULL, $flag = 0)
    {
        $result = [
            'success'   => $success,
            'message'   => $message,
            'data'      => $data,
            'flag'      => $flag
        ];

        return $result;
    }

    /**
     * pretty json encode
     * @param $data
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxOutput($data, $statusCode = 200)
    {
        return response()->json($data, $statusCode, array('Content-Type' => 'application/json'), JSON_PRETTY_PRINT);
    }

    /*
    map data array to obj
     */
    function mapData($obj, $data = [])
    {
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $obj->{$key} = $value;
            }
        }
        return $obj;
    }

    /**
     * redirect url with localization
     * @param $url
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function redirect($url)
    {
        return redirect($url)->send();
    }

    /**
     * get url with localization
     * @param $url
     * @return string
     */
    public function url($url = '', $secure = false)
    {
        
        return $secure ? secure_url($url) : url($url);
    }

    public function getThemeAssets($asset)
    {
        return asset($asset);
    }

    public function getThemeCss($css)
    {
        return $this->getThemeAssets('assets/css/'.$css);
    }

    public function getThemeJs($js)
    {
        return $this->getThemeAssets('assets/js/'.$js);
    }

    public function getThemePlugins($plugin)
    {
        return $this->getThemeAssets('assets/plugins/'.$plugin);
    }

    public function getThemeImg($img)
    {
        return $this->getThemeAssets('assets/img/'.$img);
    }

    public function getImage($fd,$photo){
         return asset($fd.'/'.$photo);
    }
    public function getGallery($photo){
        return asset('/media/gallery/'.$photo);
    }

    public function getGalleryBy($type,$id,$high = FALSE){
        $gallery = new GalleryEntity();
        switch ($type) {
            case 'home':
                $gallery = $gallery->where('type_page',6)->where('page_id',$id)->first();

                if(!empty($gallery)){
                    return asset('/media/gallery/'.$gallery->link);
                }
                return $this->getThemeAssets('img/ROSALEEN_LOGO_FINAL-1-01.png');
            break;
                
        }
        if($high){
            $gallery = $gallery->where('type_page',$type)->where('page_id',$id);
            $gallery1 = $gallery->where('highlight','1')->first();
            if(!empty($gallery1) && count($gallery1)){
                return $gallery1;
            }else{
                $gallery2 = GalleryEntity::where('type_page',$type)->where('page_id',$id);
                $gallery2 = $gallery2->first();
                if(!empty($gallery2)){
                    return $gallery2;
                }
                return $this->getThemeAssets('img/ROSALEEN_LOGO_FINAL-1-01.png');
            }
            
        }
        return GalleryEntity::where('type_page',$type)->where('page_id',$id)->get();
        
    }

    public function view_price($number){
        return number_format($number, 0, '', '.');
    }


    public function getIpAddress() {

        $ip_address = '';

        if (isset($_SERVER)) {
            $ip_address = $_SERVER["REMOTE_ADDR"];
            if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]) && ip2long($_SERVER["HTTP_X_FORWARDED_FOR"]) !== false) {
                $ip_address = $_SERVER["HTTP_X_FORWARDED_FOR"];
            } elseif (isset($_SERVER["HTTP_CLIENT_IP"]) && ip2long($_SERVER["HTTP_CLIENT_IP"]) !== false) {
                $ip_address = $_SERVER["HTTP_CLIENT_IP"];
            }
        } else {
            $ip_address = getenv('REMOTE_ADDR');
            if (getenv('HTTP_X_FORWARDED_FOR') && ip2long(getenv('HTTP_X_FORWARDED_FOR')) !== false) {
                $ip_address = getenv('HTTP_X_FORWARDED_FOR');
            } elseif (getenv('HTTP_CLIENT_IP') && ip2long(getenv('HTTP_CLIENT_IP')) !== false) {
                $ip_address = getenv('HTTP_CLIENT_IP');
            }
        }
        return $ip_address;
    }

    public function getInfoByIp($ip){
        // get geoip location data from ip
        // get country name
        $data = [];
        $geopluginURL = 'http://www.geoplugin.net/php.gp?ip=' . $ip;

        $addrDetailsArr = unserialize($this->getApiContent($geopluginURL));
        if (!$addrDetailsArr || empty($addrDetailsArr))
            return $data;

        $data['country'] = $addrDetailsArr['geoplugin_countryName'];
        $data['city'] = $addrDetailsArr['geoplugin_city'] . '-' . $addrDetailsArr['geoplugin_region'];
        $data['latitude'] = $addrDetailsArr['geoplugin_latitude'];
        $data['longitude'] = $addrDetailsArr['geoplugin_longitude'];
        return $data;
    }

    public function getApiContent($url)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2'
        ));
        $resp = curl_exec($curl);
        curl_close($curl);
        return $resp;
    }

    public function ip_info($ip)
    {
        $url = "http://api.ipinfodb.com/v3/ip-city/?key=456e40bb0807a57f4be0b40ca0cf959a75813b374346d891c47d3bf952996b04&ip={$ip}&format=json";
        $d = $this->getApiContent($url);

        $data = json_decode($d, true);

        return $data;
    }
    /** Map string array to Int array.
     * @param array $arr
     * @return array
     */
    public function stringToInt($array = [], $ex = ',')
    {

        if (!is_array($array)) {
            $array = explode($ex, $array);
        }

        $int = [];
        foreach ($array as $arr) {
            $int[] = (int) $arr;
        }
        return $int;
    }

    /**
     * compares given route name with current route name
     * @param $routes
     * @param string $output
     * @return string
     */
    public function activeRoutes($routes, $output = 'active')
    {

        $path = Route::getCurrentRoute()->uri;
        if (!is_array($routes)) {
            return (ends_with($path, $routes)) ? $output : '';
        }

        foreach ($routes as $route) {
            if (ends_with($path, $route)) return $output;
        }

        return '';
    }

    /**
     * compare given segment with values
     * @param $segment
     * @param $value
     * @return string
     */
    public function activeSegments($segment, $values, $output = 'active')
    {
        if (!is_array($values)) {
            return \Request::segment($segment) == $values ? $output : '';
        }

        foreach ($values as $value) {
            if (\Request::segment($segment) == $values) return $output;
        }

        return '';
    }

    /**
     * detects if the given string is found in the current URL.
     * @param  string  $string
     * @param  string  $output
     * @return boolean
     */
    public function activeMatch($string, $output = 'active')
    {
        return (\Request::is($string)) ? $output : '';
    }

    public function getSession($session)
    {
        if (!session()->has($session)) {
            session($session, []);
        }

        return session($session);
    }

    public function getPopup()
    {
        if (session()->has('show-popup')) {
            return true;

        }

        return false;
    }

    public function setSession($data = [],$sessName)
    {
        // set sessions to new data
        session()->put($sessName, $data);
    }

    /**
     * [simpleStatus trạng thái stt cơ bản]
     * @param  [int] $trang_thai [1= active, 2=deactive]
     * @return [button]
     */
    public function simpleStatus($trang_thai){
        switch ($trang_thai) {
            case 2:
                $status = '<span class="label label-danger">UnActive</span>';
                break;
            case 0:
                $status = '<span class="label label-warning">Undefined</span>';
                break;
            default:
                $status = '<span class="label label-success">Active</span>';
                break;
        }
        return $status;
    }
    public function highLight($trang_thai){
        switch ($trang_thai) {
            case 2:
                $status = '<span class="label label-danger">No</span>';
                break;
            default:
                $status = '<span class="label label-success">Yes</span>';
                break;
        }
        return $status;
    }

    public function view_date($date){
        return date("F j, Y, g:i a",strtotime($date));
    }

    public function getSystemConfig($whereIn = FALSE)
    {
        $system = new Setting();
        if($whereIn){
            $system = $system->whereIn('title',$whereIn);
        }
        $system = $system->get();
        $config = [];
        if (!empty($system)) {
            foreach ($system as $sys) {
                $config[$sys->title] = $sys->value;
            }
        }
        return $config;
    }


    function highlight_phrase($str, $phrase, $tag_open = '<span style="color:#e65100;">', $tag_close = '</span>')
    {
        return ($str !== '' && $phrase !== '')
            ? preg_replace('/('.preg_quote($phrase, '/').')/i'.('u'), $tag_open.'\\1'.$tag_close, $str)
            : $str;
    }

     //return ago time
    public function ago($time)
    {
        $now = time();
        $old = strtotime($time);
        $ago = $now - $old;
        if($ago < 30)
            return trans('site.just_now');
        if ($ago < 60)
            return $ago .' '. trans('site.seconds_ago');

        if ($ago < 3600)
            return (int)($ago / 60) .' '. trans('site.minute_ago');

        if ($ago < 86400)
            return (int)($ago / 3600) .' '. trans('site.hour_ago');

        if ($ago < 2592000)
            return (int)($ago / 86400) .' '. trans('site.day_ago');

        if ($ago < 31104000)
            return (int)($ago / 2592000) .' '. trans('site.month_ago');

        return (int)($ago / 31104000) .' '. trans('site.year_ago');
    }

    /**
     * protect email from spam and crawler
     * @param $email
     * @return string
     */
    public function hideEmail($email, $mailto = false)
    {
        $character_set = '+-.0123456789@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz';
        $key = str_shuffle($character_set);
        $id = 'e'.rand(1, 999999999);
        $cipher_text = '';
        for ($i = 0; $i < strlen($email); $i += 1) {
            $cipher_text .= $key[strpos($character_set, $email[$i])];
        }

        $script = 'var a="'.$key.'";var b=a.split("").sort().join("");var c="'.$cipher_text.'";var d="";';
        $script .= 'for(var e=0;e<c.length;e++)d+=b.charAt(a.indexOf(c.charAt(e)));';

        if ($mailto) {
            $script.= 'document.getElementById("'.$id.'").innerHTML="<a href=\\"mailto:"+d+"\\">"+d+"</a>"';
        } else {
            $script.= 'document.getElementById("'.$id.'").innerHTML=d';
        }

        $script = "eval(\"".str_replace(array("\\",'"'),array("\\\\",'\"'), $script)."\")";
        $script = '<script type="text/javascript">/*<![CDATA[*/'.$script.'/*]]>*/</script>';

        return '<span id="'.$id.'"></span>'.$script;
    }

    public function shortContent($content, $length = 200)
    {
        $content = strip_tags($content);
        if (strlen($content) > $length) {
            $ex = explode(' ', $content);
            $short = '';
            foreach ($ex as $string) {
                $short .= $string;
                if (strlen($short) >= $length) break;
                $short .= ' ';
            }

            return $short.'...';
        }

        return $content;
    }

    /**
     * get status class by status code
     * @param $status
     * @return string
     */
    public function statusClass($status)
    {
        switch($status) {
            case 'delete':
            case 'unactive':
            case 'disabled':
                return 'label-danger';
            case 'edit':
            case 'pending':
                return 'label-warning';
            case 'active':
            default:
                return 'label-success';
        }
    }

    /**
     * remap datatables data to correct array
     * @param array $data
     * @return array
     */
    public function convertDataTables($data = [])
    {
        $result = [];
        if (!empty($data)) {
            $result = [
                'draw'          => $data['draw'],
                'start'         => (int) $data['start'],
                'end'           => (int) $data['length'],
                'order_by'      => $data['order'][0]['column'],
                'order_sort'    => $data['order'][0]['dir'],
                'search_value'  => $data['search']['value'],
                'search_regex'  => $data['search']['regex']
            ];
        }

        return $result;
    }

    public function removeExceededSpaces($str)
    {
        return trim(preg_replace('/\s+/', ' ', $str));
    }


    public function get_type($category){
        if($category == 1){
            return "Danh sách dự án";
        }elseif($category == 2){
            return "Danh sách tin tức";
        }else{
            return "Danh sách dịch vụ";
        }
    }

    public function getFaq($where = FALSE){
        $faq = new Faq();
        if($where){
            $faq = $faq->where($where);
        }
        return $faq->get();
    }


    public function danh_muc($where = FALSE, $limit = FALSE,$order_by = FALSE)
    {
        $item = new Category();
        if (is_array($limit)) {
            $item = $item->limit($limit[0])->offset($limit[1]);
        } elseif ($limit) {
            $item = $item->limit($limit);
        }else{
            $item = $item;
        }
        if($where){
            $item = $item->where($where);
        }
        if($order_by && is_array($order_by)){
            $item = $item->orderBy($order_by[0],$order_by[1]);
        }else{
            $item = $item->orderBy('id','DESC');
        }
        return $item->get();
    }

    public function send_mail($to,$name,$subject,$message){
        if(empty($to))
            return false;
        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.yandex.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'mng-noreply@vietprojectgroup.com';                 // SMTP username
        $mail->Password = 'Vietprojectgroup!2!2';                           // SMTP password
        $mail->setFrom('mng-noreply@vietprojectgroup.com','ViTPR');
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        $mail->addAddress($to,$name);     // Add a recipient
        //$mail->addCC('cc@example.com');
        //$mail->addBCC('bcc@example.com');
        $mail->CharSet = "UTF-8";
        $mail->isHTML(true);                              // Set email format to HTML

        $mail->Subject = $subject;
        $mail->Body    = $message;
        return $mail->send();
    }
}