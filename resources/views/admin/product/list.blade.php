@extends('admin.templates.master')

@section('content')

<section class="content-header">
    <h1>Danh sách sản phẩm</h1>
    <ol class="breadcrumb">
        <li><a href="{{Helper::url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Danh sách sản phẩm</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách sản phẩm</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped table-data">
                        <thead>
                            <tr>
                                <th>Tiêu đề</th>
                                <th>Ảnh đại diện</th>
                                <th>Danh mục</th>
                                <th>Giá</th>
                                <th>Tình trạng</th>
                                <th>Nổi bật</th>
                                <th>Khuyến mãi</th>
                                <th>Trạng thái</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(!empty($item))
                            @foreach ($item as $key)
                                <tr>
                                    
                                    <td>
                                        {{$key->title}}
                                        <div class="row-actions">
                                            <span class="edit">
                                                <a href="{{Helper::url('admin/san-pham/edit/'.$key->id)}}">Edit</a> |
                                            </span>
                                            <span><a href="javascript:void(0);" class="text-red remove-item" data-id="{{$key->id}}">Delete</a></span>
                                            
                                        </div>
                                    </td>
                                    <td>
                                        <div style="width:80px; ">
                                            <img src="{{Helper::getImage('media/product',$key->img)}}" alt="{{$key->title}}" title="{{$key->title}}" style="width: 100%;height: auto;">
                                        </div>
                                    </td>
                                    <td>{{$key->category()->first()->title}}</td>
                                    <td>{{$key->price}}</td>
                                    <td>{{$key->state}}</td>
                                    <td class="text-center">
                                        <a class="change-high" href="javascript:void(0);" data-id="{{$key->id}}">
                                        {!!Helper::highLight($key->highlight)!!}
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="change-sale" href="javascript:void(0);" data-id="{{$key->id}}">
                                        {!!Helper::highLight($key->sales)!!}
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a href="javascript:void(0);" class="change-status" data-id="{{$key->id}}">{!!Helper::simpleStatus($key->status)!!}</a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="box-footer">
                    <a class="btn btn-primary" href="{{Helper::url('admin/san-pham/add')}}">Thêm mới</a>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('js_wrapper')
   @include('admin.product.list_js')
@stop
