@extends('admin.templates.master')

@section('content')
<section class="content-header">
    <h1>Contact Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{Helper::url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Contact Management</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Contact Management</h3>
                </div>
                {!! Form::open(['url' => Helper::url('admin/submit_contact'), 'id' => 'submitForm']) !!}
                <div class="box-body">
                    <div class="form-group">
                        <label>Địa chỉ:</label>
                        <input type="text" class="form-control" name="address" value="{{$meta['address']}}" required/>
                    </div>
                    <div class="form-group">
                        <label>Điện thoại:</label>
                        <input type="text" class="form-control" name="phone" value="{{$meta['phone']}}" required/>
                    </div>
                    <div class="form-group">
                        <label>Hotline:</label>
                        <input type="text" class="form-control" name="hotline" value="{{$meta['hotline']}}" required/>
                    </div>
                    <div class="form-group">
                        <label>Email:</label>
                        <input type="text" class="form-control" name="email" value="{{$meta['email']}}" required/>
                    </div>
                    <div class="form-group">
                        <label>Thời gian làm việc:</label>
                        <input type="text" class="form-control" name="hour_work" value="{{$meta['hour_work']}}" required/>
                    </div>
                    <div class="form-group">
                        <label>Map:</label>
                        <textarea class="form-control" name="map" required>{{$meta['map']}}</textarea>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
    $('#submitForm').validate({
        ignore: [],
        rules: {
        },
        messages: {
        },
        submitHandler: function(form) {
            var submit_url = $('#submitForm').attr('action');
            $.ajax({
                type: "POST",
                url: submit_url,
                data: $(form).serialize(),
                dataType: "JSON",
                beforeSend : function(){
                    $('#loading').show();
                },
                success: function(result){
                    $('#loading').hide();
                    if(result.success){
                        location.reload();
                        return false;
                    }
                    else{
                        modalError(result.message);
                        return false;
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    $('#loading').hide();
                    modalError(jqXHR.status +' '+errorThrown+'. Please reload and try agian. Thank you!!');
                    // modalError(xhr.responseText);
                }
            });
            return false;
        }
    });
});
</script>
@stop
