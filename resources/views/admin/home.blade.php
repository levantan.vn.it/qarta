@extends('admin.templates.master')

@section('content')
<link rel="stylesheet" href="{{Helper::getThemePlugins('jvectormap/jquery-jvectormap-1.2.2.css')}}" />
<link rel="stylesheet" href="{{Helper::getThemePlugins('datetimepicker/jquery.datetimepicker.css')}}" />
<link rel="stylesheet" href="{{Helper::getThemePlugins('datepicker/datepicker3.css')}}">
<style type="text/css">
    .dataTables_info{
        display: block!important;
    }
</style>
@if($current_user->group == 1)
<section class="content-header">
    <h1>Dashboard</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<section class="content">
    <h3>Hello {{$current_user->fullname()}}</h3>
</section>
@else
<section class="content-header">
    <h1>User Profile</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">User Profile</li>
    </ol>
</section>
<section class="content">
	@if($user->status == 0)
	<div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
        Your account is not authenticated. Please check the mail for confirmation. You can send mail back <a href="{{url('sendmail')}}">here</a>.
      </div>
	@endif
	<div class="box box-primary">
		<div class="box-header">
		    <h3 class="box-title">About Me</h3>
		    <div class="box-tools pull-right">
		        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		        
		    </div>
		</div>
		<div class="box-body">
		    <ul class="nav nav-stacked">
		    	<li><p>Username: <strong>{{$user->username}}</strong></p></li>
		    	<li><p>Fullname: <strong>{{$user->fullname}}</strong></p></li>
		    	<li><p>Email: <strong>{{$user->email}}</strong></p></li>
		    	<li><p>Country: <strong>{{$user->country()->first()->Country}}</strong></p></li>
		    	<li><p>State: <strong>{{$user->state()->first()->Region}}</strong></p></li>
		    	<li><p>Phone: <strong>{{$user->phone}}</strong></p></li>
		    	<li><p>Created time: <strong>{{date('m/d/Y H:i:s',strtotime($user->created_at))}}</strong></p></li>
		    	<li><p>Updated time: <strong>{{date('m/d/Y H:i:s',strtotime($user->updated_at))}}</strong></p></li>
		    </ul>
		</div>
	</div>
</section>
@endif
@stop