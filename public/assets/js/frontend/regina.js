// Slider
$(document).ready(function(){
    $('.slider-home').slick({
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav',
        autoplay: true,
    });

    $('.slider-Customer-Review').slick({
        arrows: false,
        autoplay: true,
        dots: true,
    });

    $(".tabs-Content").hashTabs();
});
/*===================================================================================================
	AURELIA - MAIN JS
===================================================================================================*/

var aurelia = (function() {
    'use strict';

    function toggleHeader() {
        var top = $(this).scrollTop(),
            header = $('.site-header');

        if (top > 40) {
            header.addClass('fixed-top');
        } else {
            header.removeClass('fixed-top');
        }
    }

    function smoothScroll(event) {
        event.preventDefault();

        var top = $(this.hash).offset().top - 70;
        $('html, body').animate({scrollTop: top}, 500);
    }

    return {
        toggleHeader: toggleHeader,
        smoothScroll: smoothScroll,
    };
})();

// WHEN DOCUMENT IS READY
$(document).ready(function() {

    $('.smooth-scroll').on('click', aurelia.smoothScroll);

    $('.filter-room-btn').on('click', function(e) {
        e.preventDefault();
        $('.filter-room-btn').removeClass('active');
        $(this).addClass('active');
    });

    $('.hotel-book-now').on('click', function() {
        $('#room-details-modal').modal('hide');
    });

    $('#mobile-menu-btn, #mobile-menu ul li a, #mobile-menu-overlay').on('click', function() {
        // e.preventDefault();
        $('#mobile-menu').css('display','block', 300);
        $('#mobile-menu-overlay').css('display','none', 300);
    });

    $('#close-mobile-menu-btn').on('click', function() {
        // e.preventDefault();
        $('#mobile-menu').css('display','none', 300);
        $('#mobile-menu-overlay').css('display','none', 300);
    });
});

$(window).on('scroll', aurelia.toggleHeader);
window.onload = function() {
    $('#page-loader').fadeOut(500);
}