<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Thank You</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
    <link rel="stylesheet" href="{{Helper::getThemeCss('admin/bootstrap.min.css')}}"/>
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"/> -->
    <link rel="stylesheet" href="{{Helper::getThemeCss('admin/font-awesome.min.css')}}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"/>
    <link rel="stylesheet" href="{{Helper::getThemeCss('admin/AdminLTE.min.css')}}"/>
    <link rel="stylesheet" href="{{Helper::getThemePlugins('iCheck/square/blue.css')}}"/>
    <link rel="stylesheet" href="{{Helper::getThemeCss('admin/style.css')}}"/>



    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            Thank You
        </div>
        <div class="login-box-body">
            <h3>Thank you register to website. Please check email to verify account</h3>
            <a href="{{url('login')}}">Login Now</a>
        </div>
    </div>
</body>
</html>