@extends('admin.templates.master')

@section('content')
<link rel="stylesheet" href="{{Helper::getThemePlugins('dmuploader/css/demo.css')}}" />
<link rel="stylesheet" href="{{Helper::getThemePlugins('dmuploader/css/uploader.css')}}" />
<section class="content-header">
    <h1>{{!empty($item)?'Cập nhật':'Thêm mới'}}</h1>
    <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{!empty($item)?'Cập nhật':'Thêm mới'}}</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">{{!empty($item)?'Cập nhật':'Thêm mới'}}</h3>
                </div>
                <div class="box-body">
                    <div class="col-xs-12">
                        {!! Form::open(['url' => url('admin/du-an/save'), 'id' => 'submitForm']) !!}
                        <div class="form">
                            @if(!empty($item))
                            <input type="hidden" name="id" value="{{$item->id}}">
                            @endif
                            <div class="form_row form-group">
                                <label>Tiêu đề:</label>
                                <input type="text" class="form_input form-control" name="title" value="{{!empty($item)?$item->title:''}}">
                            </div>
                            
                            
                            <div class="form_row form-group">
                                <label>Ảnh đại diện:<i class="text-red">*</i></label>
                                <div id="drag-and-drop-zone" class="uploader">
                                    <div class="panel-body img-panel-files" id="img-files">
                                        @if(!empty($item))
                                        <img src="{{Helper::getImage('media/news',$item->img)}}" alt="{{$item->title}}" title="{{$item->title}}">
                                        @else
                                        <span class="img-note">Upload image max size 3MB</span>
                                        @endif
                                        
                                        
                                    </div>
                                    <div class="or">OR</div>
                                    <div class="browser">
                                        <label>
                                            <span>Click to open the file Browser</span>
                                            <input type="file" name="files" accept="image/*" title="Upload Image">
                                        </label>
                                    </div>
                                </div>
                                <input type="hidden" name="img" id="featured_image" value="{{!empty($item)?$item->img:''}}"/>
                            </div>

                            <div class="form_row form-group">
                                <label>Tóm tắt chi tiết:</label>
                                <textarea rows="5" name="excerpt" class="form_input form-control">{{!empty($item)?$item->excerpt:''}}</textarea>
                            </div>

                            <div class="form_row form-group">
                                <label>Nội dung:</label>
                                <textarea rows="20" name="content" id="tinymce_editor" class="form_input form-control">{{!empty($item)?$item->content:''}}</textarea>
                            </div>
                        </div>
                        <input type="hidden" name="category" value="{{$category}}">
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="{{Helper::getThemePlugins('dmuploader/js/demo-preview.js')}}"></script>
<script src="{{Helper::getThemePlugins('dmuploader/js/dmuploader.min.js')}}"></script>
<script src="{{Helper::getThemePlugins('tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
    
    $('#submitForm').validate({
        ignore: [],
        rules: {
            title:{
                maxlength: 100,
                required: true
            },
            content:{
                required: true
            },
            featured_image:{
                required: true
            }
        },
        messages: {
        },
        submitHandler: function(form) {
            var submit_url = $('#submitForm').attr('action');
            $.ajax({
                type: "POST",
                url: submit_url,
                data: $(form).serialize(),
                dataType: "JSON",
                beforeSend : function(){
                    $('#loading').show();
                },
                success: function(result){
                    $('#loading').hide();
                    if(result.success){
                        location.replace("{{url('admin/du-an')}}");
                        return false;
                    }
                    else{
                        modalError(result.message);
                        return false;
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    $('#loading').hide();
                    modalError(jqXHR.status +' '+errorThrown+'. Please reload and try agian. Thank you!!');
                    // modalError(xhr.responseText);
                }
            });
            return false;
        }
    });

    tinymce.init({
        selector: "#tinymce_editor",
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
        },
        relative_urls: false,
        menubar: true,
        plugins: [
            "advlist autolink link image lists preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu paste textcolor responsivefilemanager"
        ],
        toolbar1: " formatselect fontselect fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        toolbar2: "preview fullscreen | forecolor backcolor | responsivefilemanager | code",
        image_advtab: true,

        external_filemanager_path: "/filemanager/",
        filemanager_title: "Responsive Filemanager",
        external_plugins: {"filemanager": "/filemanager/plugin.min.js"}
    });

    $('#drag-and-drop-zone').dmUploader({
        url: "{{Helper::url('admin/upload_tmp')}}",
        dataType: 'json',
        extraData: {
            "_token": "{{ csrf_token() }}"
        },
        allowedTypes: 'image/*',
        onNewFile: function (id, file) {
            var i = id - 1;
            $('#img-files').find('#img-file' + i).remove();
            $.danidemo.addFile('#img-files', id, file);

            /*** Begins Image preview loader ***/
            if (typeof FileReader !== "undefined") {
                var reader = new FileReader();

                var img = $('#img-files').find('.image-preview').eq(0);
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                };
                reader.readAsDataURL(file);
            } else {

                $('#img-files').find('.image-preview').remove();
            }
            /*** Ends Image preview loader ***/
        },
        onUploadProgress: function (id, percent) {
            var percentStr = percent + '%';
            $.danidemo.updateFileProgress(id, percentStr);
        },
        onUploadSuccess: function(id, data){
            if (data.success) {
                $('#featured_image').val(data.data);
                $('#img-files').find('p').remove();
            }
            else{
                $('#img-files').html(data.data);
            }
        }
    });
});
</script>
@stop
