@extends('admin.templates.master')

@section('content')
<link rel="stylesheet" href="{{Helper::getThemePlugins('dmuploader/css/demo.css')}}" />
<link rel="stylesheet" href="{{Helper::getThemePlugins('dmuploader/css/uploader.css')}}" />
<section class="content-header">
    <h1>{{!empty($item)?'Cập nhật':'Thêm mới'}}</h1>
    <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('admin/san-pham')}}"> Danh sách</a></li>
        <li class="active">{{!empty($item)?'Cập nhật':'Thêm mới'}}</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">{{!empty($item)?'Cập nhật':'Thêm mới'}}</h3>
                </div>
                <div class="box-body">
                    <div class="col-xs-12">
                        {!! Form::open(['url' => url('admin/san-pham/save'), 'id' => 'submitForm']) !!}
                        <div class="form">
                            @if(!empty($item))
                            <input type="hidden" name="id" value="{{$item->id}}">
                            @endif
                            <div class="form_row form-group">
                                <label>Danh mục:<span class="text-red">*</span></label>
                                {!!Form::select('category_id', $category, !empty($item)?$item->category_id:null,['class' => 'form-control','required']);!!}
                            </div>
                            <div class="form_row form-group">
                                <label>Tiêu đề:<span class="text-red">*</span></label>
                                <input type="text" class="form_input form-control" name="title" value="{{!empty($item)?$item->title:''}}" required>
                            </div>
                            
                            <div class="form_row form-group">
                                <label>Ảnh đại diện:<i class="text-red">*</i></label>
                                <div id="drag-and-drop-zone" class="uploader">
                                    <div class="panel-body img-panel-files" id="img-files">
                                        @if(!empty($item))
                                        <img src="{{Helper::getImage('media/category',$item->img)}}" alt="{{$item->title}}" title="{{$item->title}}">
                                        @else
                                        <span class="img-note">Upload image max size 3MB</span>
                                        @endif
                                        
                                        
                                    </div>
                                    <div class="or">OR</div>
                                    <div class="browser">
                                        <label>
                                            <span>Click to open the file Browser</span>
                                            <input type="file" name="files" accept="image/*" title="Upload Image">
                                        </label>
                                    </div>
                                </div>
                                <input type="hidden" name="img" id="featured_image" value="{{!empty($item)?$item->img:''}}"/>
                            </div>

                            <div class="form_row form-group">
                                <label>Kích thước:</label>
                                <input type="text" class="form_input form-control" name="size" value="{{!empty($item)?$item->size:''}}">
                            </div>

                            <div class="form_row form-group">
                                <label>Tình trạng:</label>
                                <input type="text" class="form_input form-control" name="state" value="{{!empty($item)?$item->state:''}}">
                            </div>

                            <div class="form_row form-group">
                                <label>Giá:</label>
                                <input type="text" class="form_input form-control" name="price" value="{{!empty($item)?$item->price:''}}">
                            </div>

                             <div class="form_row form-group">
                                <label>Giá cũ:</label>
                                <input type="text" class="form_input form-control" name="old_price" value="{{!empty($item)?$item->price:''}}">
                            </div>

                            <div class="form-group">
                                <label>Chi tiết:<span class="text-red">*</span></label>
                                <textarea class="form-control" name="content" id="tinymce_editor" rows="20" required>{{!empty($item)?$item->content:''}}</textarea>
                            </div>

                            <div class="box box-solid">
                                <div class="box-body">
                                    <table width="100%">
                                        <tbody>
                                            <tr>
                                                <td>
                                                <div class="att-one">
                                                @if(!empty($gallery))
                                                    @foreach ($gallery as $key)
                                                    
                                                    <div class="fl">
                                                        <div class="bugdet_attach">
                                                            <table width="100%" height="100%">
                                                               <tbody>
                                                                   <tr>
                                                                       <td align="center">
                                                                        <img  src="{{Helper::getGallery($key->link)}}" alt="{{$key->name}}" title="{{$key->name}}" class="img-responsive img-b">
                                                                       </td>
                                                                   </tr>
                                                               </tbody> 
                                                            </table>
                                                        </div>
                                                        <div class="bug-des">
                                                            <div>{{$key->name}}</div>
                                                            <div><a href="javascript:void(0);" class="del-gallery" data-id="{{$key->id}}">Delete</a>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                @endif
                                                </div>
                                                </td>
                                            </tr>
                                        </tbody>  
                                    </table>
                                </div>
                                <div class="box-body">
                                    <div class="form_row form-group">
                                        <div class="dropzone media" id="add-media"></div>
                                    </div> 
                                </div>
                            </div>

                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="{{Helper::getThemePlugins('dmuploader/js/demo-preview.js')}}"></script>
<script src="{{Helper::getThemePlugins('dmuploader/js/dmuploader.min.js')}}"></script>
<script src="{{Helper::getThemePlugins('tinymce/tinymce.min.js')}}"></script>
<script src="{{Helper::getThemePlugins('dropzone/dist/min/dropzone.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function(){
    
    $('#submitForm').validate({
        ignore: [],
        rules: {
        },
        messages: {
        },
        submitHandler: function(form) {
            var submit_url = $('#submitForm').attr('action');
            $.ajax({
                type: "POST",
                url: submit_url,
                data: $(form).serialize(),
                dataType: "JSON",
                beforeSend : function(){
                    $('#loading').show();
                },
                success: function(result){
                    $('#loading').hide();
                    if(result.success){
                        location.replace("{{url('admin/san-pham')}}");
                        return false;
                    }
                    else{
                        modalError(result.message);
                        return false;
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    $('#loading').hide();
                    modalError(jqXHR.status +' '+errorThrown+'. Please reload and try agian. Thank you!!');
                    // modalError(xhr.responseText);
                }
            });
            return false;
        }
    });

    $('#drag-and-drop-zone').dmUploader({
        url: "{{Helper::url('admin/upload_tmp')}}",
        dataType: 'json',
        extraData: {
            "_token": "{{ csrf_token() }}"
        },
        allowedTypes: 'image/*',
        onNewFile: function (id, file) {
            var i = id - 1;
            $('#img-files').find('#img-file' + i).remove();
            $.danidemo.addFile('#img-files', id, file);

            /*** Begins Image preview loader ***/
            if (typeof FileReader !== "undefined") {
                var reader = new FileReader();

                var img = $('#img-files').find('.image-preview').eq(0);
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                };
                reader.readAsDataURL(file);
            } else {

                $('#img-files').find('.image-preview').remove();
            }
            /*** Ends Image preview loader ***/
        },
        onUploadProgress: function (id, percent) {
            var percentStr = percent + '%';
            $.danidemo.updateFileProgress(id, percentStr);
        },
        onUploadSuccess: function(id, data){
            if (data.success) {
                $('#featured_image').val(data.data);
                $('#img-files').find('p').remove();
            }
            else{
                $('#img-files').html(data.data);
            }
        }
    });

    tinymce.init({
        selector: "#tinymce_editor",
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
        },
        relative_urls: false,
        menubar: true,
        plugins: [
            "advlist autolink link image lists preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu paste textcolor responsivefilemanager"
        ],
        toolbar1: " formatselect fontselect fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        toolbar2: "preview fullscreen | forecolor backcolor | responsivefilemanager | code",
        image_advtab: true,

        external_filemanager_path: "/filemanager/",
        filemanager_title: "Responsive Filemanager",
        external_plugins: {"filemanager": "/filemanager/plugin.min.js"}
    });

    Dropzone.autoDiscover = false;
    var mydropzone = new Dropzone("#add-media",{
        url: "{{ Helper::url('admin/upload_tmp')}}",
        addRemoveLinks : true,
        acceptedFiles: "image/*",
        maxFilesize: 30,
        dictDefaultMessage: '<span class="text-center"><span class="font-lg visible-xs-block visible-sm-block visible-lg-block"><span class="font-lg"><i class="fa fa-cloud-upload" aria-hidden="true"></i> Drag & Drop files here</span><span>&nbsp&nbsp<h4 class="display-inline"> (Or click)</h4></span>',
        dictResponseError: 'Error uploading file!',
        params: {
            _token: "{{ csrf_token() }}",
        },
        dictRemoveFile:'Delete',
        init: function () {
            this.on("removedfile",function(file){
                var name = file.id;
                if(name == null)
                    return false;
                $("input[name='gallery["+name+"]']").remove();
                $.ajax({
                    type: "POST",
                    url: "{{Helper::url('admin/remove_tmp')}}",
                    data: {
                        name: name,
                        "_token": "{{ csrf_token() }}"
                    },
                    dataType: "JSON",
                    beforeSend : function(){
                    },
                    success: function(result) {
                        return false;
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        modalError(jqXHR.status +' '+errorThrown+'. Please reload and try agian. Thank you!!');
                        // modalError(xhr.responseText);
                    }
                });
            });
            this.on("success",function(file,response){
                $("#add-media").append("<input type='hidden' id='"+response.data+"' value='"+response.flag+"' name='gallery["+response.data+"]'/>");
                file.id  = response.data;
            });
        }
    });

    $(document).on('click','.del-gallery',function(){
        var obj = $(this);
        var id = obj.attr('data-id');
        var dialog = new Messi(
            "Do you want remove this gallery",
            {
                modal: true,
                modalOpacity: 0.5,
                title: 'Confirmation',
                titleClass: 'warning',
                buttons: [
                    {id: 0, label: 'Yes', val: 'Y'},
                    {id: 1, label: 'No', val: 'N'}
                ],
                callback: function(val) {
                    if (val == 'Y') {
                        $.ajax({
                            type: "POST",
                            url: "{{Helper::url('admin/removeGallery')}}",
                            data: {
                                id: id,
                                "_token": "{{ csrf_token() }}"
                            },
                            dataType: "JSON",
                            beforeSend : function(){
                                $('#loading').show();
                            },
                            success: function(result) {
                                $('#loading').hide();
                                if (result.success){
                                    obj.parents('.fl').remove();
                                    $('#'+id+'').remove();
                                }else{
                                    modalError(result.message);
                                }
                                return false;
                            },
                            error: function(jqXHR, textStatus, errorThrown){
                                $('#loading').hide();
                                modalError(jqXHR.status +' '+errorThrown+'. Please reload and try agian. Thank you!!');
                                // modalError(xhr.responseText);
                            }
                        });
                    }
                }
            }
        );
    });
});
</script>
@stop
