@extends('admin.templates.master')

@section('content')

<section class="content-header">
    <h1>Danh mục sản phẩm</h1>
    <ol class="breadcrumb">
        <li><a href="{{Helper::url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Danh mục sản phẩm</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh mục sản phẩm</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped table-data">
                        <thead>
                            <tr>
                                <th>Tiêu đề</th>
                                <th>Ảnh đại diện</th>
                                <th>Số lượng sản phẩm</th>
                                <th>Ngày tạo</th>
                                <th>Trạng thái</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(!empty($item))
                            @foreach ($item as $key)
                                <tr>
                                    
                                    <td>
                                        {{$key->title}}
                                        <div class="row-actions">
                                            <span class="edit">
                                                <a href="{{Helper::url('admin/san-pham/danh-muc/edit/'.$key->id)}}">Edit</a> |
                                            </span>
                                            <span><a href="javascript:void(0);" class="text-red remove-item" data-id="{{$key->id}}">Delete</a></span>
                                            
                                        </div>
                                    </td>
                                    <td>
                                        <div style="width:80px; ">
                                            <img src="{{Helper::getImage('media/category',$key->img)}}" alt="{{$key->title}}" title="{{$key->title}}" style="width: 100%;height: auto;">
                                        </div>
                                    </td>
                                    <td>{{$key->product()->where('status',1)->count()}}</td>
                                    <td>{{date('d/m/Y H:i',strtotime($key->created_at))}}</td>
                                    <td class="text-center">
                                        <a href="javascript:void(0);" class="change-status" data-id="{{$key->id}}">{!!Helper::simpleStatus($key->status)!!}</a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="box-footer">
                    <a class="btn btn-primary" href="{{Helper::url('admin/san-pham/danh-muc/add')}}">Thêm mới</a>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('js_wrapper')
   @include('admin.category.list_js')
@stop
