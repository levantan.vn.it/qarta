@extends('admin.templates.blank')
@section('content')
<style type="text/css">
    label.error{
        top: 0px!important;
    }
</style>
<div class="">
    <div class="modalTitle">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title text-blue"><i class="fa fa-asterisk"></i>&nbsp;&nbsp;Edit User</h4>
    </div>
    {!! Form::open(['url' => Helper::url('edit_user'), 'id' => 'addTeam']) !!}
        <div class="box-body">
            <div class="form">
                <input type="hidden" name="id" value="{{$user->id}}">
                <div class="form_row form-group">
                    <label>Email:<span class="text-red">*</span></label>
                    <input class="form-control" name="email" readonly="true" value="{{$user->email}}" />
                </div>
                <div class="form_row form-group">
                    <label>First Name:<span class="text-red">*</span></label>
                    <input class="form-control" name="firstname"  value="{{$user->firstname}}" />
                </div>
                <div class="form_row form-group">
                    <label>Last Name:<span class="text-red">*</span></label>
                    <input class="form-control" name="lastname" value="{{$user->lastname}}" />
                </div>
                
                @if(!empty($country) && count($country))
                <div class="form-group">
                    <select class="form-control" id="country" name="country">
                        <option value>Country</option>
                        @foreach($country as $key)
                        <option value="{{$key->id}}" {{$key->id == $user->country?'selected':''}}>{{$key->Country}}</option>
                        @endforeach
                    </select>
                </div>
                @endif

                @if(!empty($state) && count($state))
                <div class="form-group">
                    <select class="form-control" id="state" name="state">
                        <option value>State</option>
                        @foreach($state as $key)
                        <option value="{{$key->id}}" {{$key->id == $user->state?'selected':''}}>{{$key->Region}}</option>
                        @endforeach
                    </select>
                </div>
                @endif
                <div class="form_row form-group">
                    <label>Phone:<span class="text-red">*</span></label>
                    <input class="form-control" name="phone" value="{{$user->phone}}" />
                </div>
            </div>
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-primary">Save</button>
            <a class="btn btn-default" data-dismiss="modal" aria-label="Close" style="margin-left: 10px">Cancel</a>
        </div>
    {!! Form::close() !!}
</div><!-- /.modal-content -->
<script type="text/javascript">
    $(document).ready(function() {
        $('#country').on('change',function(){
            var id = $(this).val();
            console.log(id);
            $.ajax({
                url: "{{url('get_state')}}",
                type: 'POST',
                dataType: 'JSON',
                data: {
                    id:id,
                    "_token": "{{csrf_token()}}"
                },
                beforeSend : function(){
                    $('#loading').show();
                    $('.btn-primary').addClass('disabled');$('.btn-primary').attr('disabled','disabled');
                },
                success: function(str) {
                    $('#loading').hide();$('.btn-primary').removeClass('disabled');$('.btn-primary').removeAttr('disabled');
                    if(str.success){
                        $('#state').html(str.data);
                    }
                    return false;
                }
            });
        });

        $('#addTeam').validate({
            ignore: [],
            rules: {
                firstname: {
                    required: true,
                    maxlength: 100,
                },
                lastname: {
                    required: true,
                    maxlength: 100,
                },
                email: {
                    required: true,
                    maxlength: 100,
                    email:true,
                },
                phone: {
                    required: true,
                    maxlength: 100,
                    number:true
                },
                country: {
                    required: true
                },
                state: {
                    required: true
                },
                password: {
                    required: true,
                    maxlength: 20,
                    minlength:6
                },
                repassword: {
                    required: true,
                    equalTo: '#password'
                }
            },
            messages: {
                username:{
                    remote: "Username is already in use"
                },
                email:{
                    remote: "Email is already in use"
                }
            },
            submitHandler: function (form) {
                var submit_url = $('#addTeam').attr('action');
                $.ajax({
                    url: submit_url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(form).serialize(),
                    beforeSend : function(){
                        $('#loading').show();
                        $('.btn-primary').addClass('disabled');$('.btn-primary').attr('disabled','disabled');
                    },
                    success: function(str) {
                        $('#loading').hide();$('.btn-primary').removeClass('disabled');$('.btn-primary').removeAttr('disabled');
                        $('.form-group').removeClass('has-error');
                        if (!str.success) {
                            modalError(str.message);
                        } else {
                            
                            location.reload();
                        }
                        return false;
                    }
                });
                return false;
            }
        });
    });
</script>
@stop