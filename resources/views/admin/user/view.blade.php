@extends('admin.templates.blank')
@section('content')

<div class="box box-widget widget-user-2">
    <div class="widget-user-header bg-aqua-active">
        <h3 class="widget-user-username" style="margin-left: 0px;">Information</h3>
    </div>
    <div class="box-footer no-padding">
        <ul class="nav nav-stacked">
            <li><p>First name: <strong>{{$user->firstname}}</strong></p></li>
            <li><p>Last name: <strong>{{$user->lastname}}</strong></p></li>
            <li><p>Email: <strong>{{$user->email}}</strong></p></li>
            <li><p>Country: <strong>{{$user->country()->first()->Country}}</strong></p></li>
            <li><p>State: <strong>{{$user->state()->first()->Region}}</strong></p></li>
            <li><p>Phone: <strong>{{$user->phone}}</strong></p></li>
            <li><p>Created time: <strong>{{date('m/d/Y H:i:s',strtotime($user->created_at))}}</strong></p></li>
            <li><p>Updated time: <strong>{{date('m/d/Y H:i:s',strtotime($user->updated_at))}}</strong></p></li>
            
        </ul>
        
    </div>
    
</div>
@stop