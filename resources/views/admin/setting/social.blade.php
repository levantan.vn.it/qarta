@extends('admin.templates.master')

@section('content')
<section class="content-header">
    <h1>Social Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{Helper::url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Social Management</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Social Management</h3>
                </div>
                {!! Form::open(['url' => Helper::url('admin/submit_social'), 'id' => 'submitForm']) !!}
                <div class="box-body">
                    <div class="form-group">
                        <label>Facebook:<i class="text-red">*</i></label>
                        <input type="text" name="facebook" class="form-control" value="{{$social['facebook']}}" required>
                    </div>
                    <div class="form-group">
                        <label>Tripadvisor:<i class="text-red">*</i></label>
                        <input type="text" name="tripadvisor" class="form-control" value="{{$social['tripadvisor']}}" required>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
    $('#submitForm').validate({
        ignore: [],
        rules: {
        },
        messages: {
        },
        submitHandler: function(form) {
            var submit_url = $('#submitForm').attr('action');
            $.ajax({
                type: "POST",
                url: submit_url,
                data: $(form).serialize(),
                dataType: "JSON",
                beforeSend : function(){
                    $('#loading').show();
                },
                success: function(result){
                    $('#loading').hide();
                    if(result.success){
                        location.reload();
                        return false;
                    }
                    else{
                        modalError(result.message);
                        return false;
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    $('#loading').hide();
                    modalError(jqXHR.status +' '+errorThrown+'. Please reload and try agian. Thank you!!');
                    // modalError(xhr.responseText);
                }
            });
            return false;
        }
    });
});
</script>
@stop
