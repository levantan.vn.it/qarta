<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Contact Us</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
    <link rel="stylesheet" href="{{Helper::getThemeCss('admin/bootstrap.min.css')}}"/>
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"/> -->
    <link rel="stylesheet" href="{{Helper::getThemeCss('admin/font-awesome.min.css')}}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"/>
    <link rel="stylesheet" href="{{Helper::getThemeCss('admin/AdminLTE.min.css')}}"/>
    <link rel="stylesheet" href="{{Helper::getThemePlugins('iCheck/square/blue.css')}}"/>
    <link rel="stylesheet" href="{{Helper::getThemeCss('admin/style.css')}}"/>



    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            Contact Us
        </div>
        <div class="login-box-body">
            <div class="alert alert-success alert-dismissable" style="display: none;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                Submit a query form to admin success.
            </div>
            {!! Form::open(['url' => Helper::url('contact'), 'id' => 'submitForm']) !!}
            <div class="form-group">
                <label>FullName:</label>
                <input class="form-control" name="fullname" value="" required="" />
            </div>
            <div class="form-group">
                <label>Email:</label>
                <input class="form-control" name="email" value="" required="" />
            </div>
            <div class="form-group">
                <label>Subject:</label>
                <input class="form-control" name="subject" value="" required="" />
            </div>
            <div class="form-group">
                <label>Message:</label>
                <textarea class="form-control" name="content" required rows="4"></textarea>
            </div>
            <div class="row">
                <div class="col-xs-12" style="text-align: center;">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
                </div>
            </div>
            {!! Form::close() !!}
            <a href="{{url('login')}}" style="display: block;padding-top: 10px;">Login</a>
            <a href="{{url('register')}}" style="display: block;">Register</a>
        </div>
    </div>
    <style type="text/css">
        #loading {z-index: 999999;position: fixed;width: 100%;height: 100%;left: 0;top: 0;background-color: rgba(0, 0, 0, 0.6);}
        .svg-icon-loader {width: 100px;height: 100px;float: left;line-height: 100px;text-align: center;}
        #loading .svg-icon-loader {position: absolute; top: 50%;left: 50%;margin: -50px 0 0 -50px;
        }
        #loading .svg-icon-loader img{
            width: 100px!important
        }
    </style>
    <div id="loading" style="display: none;">
        <div class="svg-icon-loader">
            <img src="{{Helper::getThemeImg('curved-bar.gif')}}" width="200px" alt="">
        </div>
    </div>
    <script src="{{Helper::getThemePlugins('jQuery/jQuery-2.1.4.min.js')}}"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script> -->
    <script src="{{Helper::getThemeJs('admin/bootstrap.min.js')}}"></script>
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> -->
    <script src="{{Helper::getThemePlugins('iCheck/icheck.min.js')}}"></script>
    <script src="{{Helper::getThemeJs('admin/jquery.validate.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#submitForm').validate({
                ignore: [],
                rules: {
                },
                messages: {
                },
                submitHandler: function(form) {
                    var submit_url = $('#submitForm').attr('action');
                    $.ajax({
                        type: "POST",
                        url: submit_url,
                        data: $(form).serialize(),
                        dataType: "JSON",
                        beforeSend : function(){
                            $('#loading').show();
                        },
                        success: function(result){
                            $('#loading').hide();
                            if(result.success){
                                // location.reload();
                                $('.alert-success').show();
                                $('input').val('');
                                $('textarea').val('');
                                return false;
                            }
                            else{
                                modalError(result.message);
                                return false;
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            $('#loading').hide();
                            modalError(jqXHR.status +' '+errorThrown+'. Please reload and try agian. Thank you!!');
                            // modalError(xhr.responseText);
                        }
                    });
                    return false;
                }
            });
        });
    </script>

</body>
</html>