@extends('admin.templates.master')

@section('content')
<section class="content-header">
    <h1>Meta Management</h1>
    <ol class="breadcrumb">
        <li><a href="{{Helper::url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Meta Management</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Meta Management</h3>
                </div>
                {!! Form::open(['url' => Helper::url('admin/submit_seo_meta'), 'id' => 'submitForm']) !!}
                <div class="box-body">
                    <div class="form-group">
                        <div class="form-group">
                        <label>Title site:</label>
                            <input type="text" class="form-control" name="title_site" value="{{$meta['title_site']}}" required/>
                        </div>
                        <label>Keywords:</label>
                        <textarea class="form-control" name="meta_keywords"  required>{{$meta['meta_keywords']}}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Title:</label>
                        <textarea class="form-control" name="meta_title" required>{{$meta['meta_title']}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="facebook_id">Description:</label>
                        <textarea class="form-control" rows="4" name="meta_description"  required>{{$meta['meta_description']}}</textarea>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
    $('#submitForm').validate({
        ignore: [],
        rules: {
        },
        messages: {
        },
        submitHandler: function(form) {
            var submit_url = $('#submitForm').attr('action');
            $.ajax({
                type: "POST",
                url: submit_url,
                data: $(form).serialize(),
                dataType: "JSON",
                beforeSend : function(){
                    $('#loading').show();
                },
                success: function(result){
                    $('#loading').hide();
                    if(result.success){
                        location.reload();
                        return false;
                    }
                    else{
                        modalError(result.message);
                        return false;
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    $('#loading').hide();
                    modalError(jqXHR.status +' '+errorThrown+'. Please reload and try agian. Thank you!!');
                    // modalError(xhr.responseText);
                }
            });
            return false;
        }
    });
});
</script>
@stop
