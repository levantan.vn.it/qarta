@extends('admin.templates.master')

@section('content')
<section class="content-header">
    <h1>{{$title}}</h1>
    <ol class="breadcrumb">
        <li><a href="{{Helper::url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{$title}}</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                {!! Form::open(['url' => Helper::url('admin/submit_cms'), 'id' => 'submitForm']) !!}
                <div class="box-body">
                    <div class="form-group">
                        <input type="hidden" name="title" value="{{$cms}}">
                        <label>Nội dung:</label>
                        <textarea class="form-control" name="value" id="tinymce_editor" rows="20">{{$content_cms}}</textarea>
                    </div>
                    
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
<script src="{{Helper::getThemePlugins('tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#submitForm').validate({
        ignore: [],
        rules: {
        },
        messages: {
        },
        submitHandler: function(form) {
            var submit_url = $('#submitForm').attr('action');
            $.ajax({
                type: "POST",
                url: submit_url,
                data: $(form).serialize(),
                dataType: "JSON",
                beforeSend : function(){
                    $('#loading').show();
                },
                success: function(result){
                    $('#loading').hide();
                    if(result.success){
                        location.reload();
                        return false;
                    }
                    else{
                        modalError(result.message);
                        return false;
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    $('#loading').hide();
                    modalError(jqXHR.status +' '+errorThrown+'. Please reload and try agian. Thank you!!');
                    // modalError(xhr.responseText);
                }
            });
            return false;
        }
    });

    tinymce.init({
        selector: "#tinymce_editor",
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
        },
        relative_urls: false,
        menubar: true,
        plugins: [
            "advlist autolink link image lists preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu paste textcolor responsivefilemanager"
        ],
        toolbar1: " formatselect fontselect fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        toolbar2: "preview fullscreen | forecolor backcolor | responsivefilemanager | code",
        image_advtab: true,

        external_filemanager_path: "/filemanager/",
        filemanager_title: "Responsive Filemanager",
        external_plugins: {"filemanager": "/filemanager/plugin.min.js"}
    });
});
</script>
@stop
